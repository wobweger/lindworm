#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      module initialization
#               
# Author:       Walter Obweger
#
# Created:      20191223
# CVS-ID:       $Id$
# Copyright:    (c) 2019 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

__version__ = "0.1.1"
