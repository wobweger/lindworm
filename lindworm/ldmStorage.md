## class ldmStorage

Class ldmStorage is base container for complex processing tasks.
Several definition can be queued and executed in sequence.
Each definition generate data set to be access later.

### functions

+ [prcBeg](#prcBeg), add definition to queue
+ [prcExc](#prcExc), get first pending queue item and preform task
+ [prcEnd](#prcEnd), remove definition from queue


### data

definition related data structures

+ [definition](#datDef), definition data
+ [data](#datDat), processed data
+ [data end](#datEnd), recently finalize definition and processed data
+ [configuration](#datCfg), configuration

#### definition{#datDef}

definition related data structures,
queued definitions, see [prcBeg](#prcBeg)

+ [lDef](#lDef), definitions
+ [dRef](#dRef), definition references
+ [dArg](#dArg), definition keyword arguments passed at [prcBeg](#prcBeg)

example:

```python
kwargs={}
self.prcBeg('def0',oDat={},oRef=oRef0,**kwargs)
```

```python
self.lDef=['def0']
self.dRef={
    'def0':oRef0
    }
self.dArg={
    'def0':{}
    }
```

#### data{#datDat}

processed data of requested definition, see [prcExc](#prcExc).

example:

```python
kwargs={}
self.prcBeg('def0',oDat={},oRef=oRef0,**kwargs)

args=()
kwargs={}
iRet=self.prcExc(*args,**kwargs)
args=()
kwargs={}
iRet=self.prcEnd(*args,**kwargs)
```

```python
self.dDat={
    'def0':{ ... }
    }
```

#### data end{#datEnd}

instance properties are updated to memorize recently processing data,
set by method [prcEnd](#prcEnd).

+ sDefEnd, definition
+ oArgEnd, keyword arguments passed at [prcBeg](#prcBeg)
+ oDatEnd, processed result
+ oRefEnd, reference data

#### data processing{#datPrc}

processing data, very internal

+ iAct, actual offset

#### configuration {#datCfg}

dictionary to configure functions.

+ dCfg, is loadable by json file, start with `None`
+ dCfgDft, defines default configuration at class level,
  use method `__initCfg__` to define it

```python
self.dCfgDft={
    'iModeRev':0,
    'lAtr':['dDat'],
    }
```

##### configuration iModeRev{#iModeRev}

`iModeRev` define mode of definition processing.
Value set usually in constructor.

+ 0, FIFO, first in first out
+ 1, FILO, first in last out

##### configuration lAtr{#lAtr}

object attribute list save

### interfaces

+ [prcBeg](#prcBeg), queue definition
+ [prcExc](#prcExc), process queued definition
+ [prcEnd](#prcEnd), finalize processing queue definition

json files are used.

+ [loadCfg](#loadCfg), load configuration [dCfg](#dCfg)
+ [getCfg](#getCfg), retrieve configuration value

#### method bldDat{#bldDat}

build data object of definition.
overwrite method in case a non dictionary object is required.

+ parameter
  + self, instance reference
  + sDef, definition
  + oRef, reference object, added to [dRef](#dRef) sDef used as key
  * \*\*kwargs, keyword arguments, added to [dArg](#dArg) sDef used as key
+ return code
  + empty dictionary

#### method prcBeg{#prcBeg}

queue definition, if not already added,
determined by [prcIsDone](#prcIsDone).
data object used during processing is delivered by [bldDat](#bldDat).

+ parameter
  + self, instance reference
  + sDef, definition
  + oDat, data object, added to [dDat](#dDat) sDef used as key
  + oRef, reference object, added to [dRef](#dRef) sDef used as key
  * \*\*kwargs, keyword arguments, added to [dArg](#dArg) sDef used as key
+ return code
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error

#### method prcIsDone{#prcIsDone}

check definition already queued.
[dDat](#datDat) is used to determine processing status.

+ parameter
  + self, instance reference
  + sDef, definition
+ return code
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error

#### method prcExc {#prcExc}

get queued definition and perform execution.
configuration [iModeRev](#iModeRev)

+ parameter
  + self, instance reference
  + \*args, flexible arguments
  + \*\*kwargs, flexible keyword arguments
+ return code
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error

#### method prcEnd {#prcEnd}

end definition processing,
current definition is removed according [iModeRev](#iModeRev).

+ parameter
  + self, instance reference
  + \*args, flexible arguments
  + \*\*kwargs, flexible keyword arguments
+ return code
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error

instance properties are updated to memorize recently processing data.
values stay until next call of [prcEnd](#prcEnd) or [clrEnd](#clrEnd).

#### method saveDat{#saveDat}

save data to json

+ parameter
  + self, instance reference
  + oDat, data save using json.dumps
  + sFN, file name
  + sDN, directory name
  + lKeys, keys of object attribute to save, attribute name and key is used as filename suffix,
    default is None
    if None, complete attribute is saved
  + sAtr, pbject attribute to save
+ return code
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error

#### method saveAtr{#saveAtr}

save attributes to json

+ parameter
  + self, instance reference
  + oDat, data save using json.dumps
  + sFN, file name
  + sDN, directory name
  + lAtr, object attribute list to save, attribute name is used as filename suffix,
    default is None
    if None, configuration [lAtr](#lAtr) is saved
+ return code
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error

#### method saveJson{#saveJson}

save data to json

+ parameter
  + self, instance reference
  + oDat, data save using json.dumps
  + sFN, file name
  + sDN, directory name
  + sSfx, file name suffix
+ return code
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error

#### method clrAll{#clrAll}

clear all instance properties.

#### method clrDef{#clrDef}

clear instance properties related to [definitions](#datDef).

#### method clrDat{#clrDat}

clear instance properties related to [processed data](#datDat).

#### method clrEnd{#clrEnd}

clear end instance properties related to [prcEnd](#datEnd).

+ sDefEnd, definition
+ oArgEnd, keyword arguments passed at [prcBeg](#prcBeg)
+ oDatEnd, processing result
+ oRefEnd, reference data

#### method loadCfg {#loadCfg}

+ parameter
  + self, instance reference
  + sCfgFN, file name
+ return code
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error

#### method getCfg {#getCfg}

get configuration value with defaults

+ parameter
  + self, instance reference
  + sKey, key to get
+ return code
  + 2, okay found in loaded configuration
  + 1, okay found in defaults
  + -1, exception
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error
+ return configuration value
  + None, unresolved key
  + else, value found

#### method getLstDef{#getLstDef}

get list of definitions.

#### method getDef{#getDef}

get definitions to process.

+ return code
  + 1, okay found definition
  + -1, exception
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error
+ return definition value

#### method getDat{#getDat}

get data for definition.

+ parameter
  + self, instance reference
  + sDef, definition, if None, get first definition to process
+ return code
  + 1, okay found definition
  + -1, exception
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error
+ return data value

#### method getRef{#getRef}

get reference for definition.

+ parameter
  + self, instance reference
  + sDef, definition, if None, get first definition to process
+ return code
  + 1, okay found definition
  + -1, exception
  + >0, okay processing done
  + =0, okay no operation (nop)
  + <0, error
+ return reference value

#### method __init__ {#__init__}

constructor.

+ parameter
  + self, instance reference
  + iModeRev, mode reverse [iModeRev](#iModeRev)


#### method __initCfg__ {#__initCfg__}

initialize [configuration data](#datCfg).

#### method __initDef__{#__initDef__}

initialize [definition data](#datDef).

#### method __initDat__{#__initDat__}

initialize [data structures](#datDat)

#### method __initPrc__{#__initPrc__}

initialize [processing structures](#datPrc)
