#----------------------------------------------------------------------------
# Name:         ldmTypes.py
# Purpose:      ldmTypes.py
#               lazy types
# Author:       Walter Obweger
#
# Created:      20210813
# CVS-ID:       $Id$
# Copyright:    (c) 2021 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------


IntType=type(1)
StringType=type('')
DictType=type({})
ListType=type([])
TupleType=type(())
