#FROM python:latest
#FROM python:3.8.13-slim
FROM python:3.7.13-slim

LABEL maintainer="Walter Obweger"
#LABEL website="https://www.vidarc.com/"

RUN apt-get update
RUN apt-get install -y build-essential libgtk-3-dev
RUN pip install -U -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/debian-9 wxPython