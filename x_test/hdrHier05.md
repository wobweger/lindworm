# background

pyGather markdown to pandoc

+ [class0](hdrFlat00.md)
+ [class0](hdrFlat01.md)
+ [class0](hdrFlat02.md)


## target

some deep thoughts

## means

something gentle

|name | comment |
|---    |---    |
| [class0](hdrFlat00.md) | base |
| [class0](hdrFlat01.md) | simple class for base |
| [class0](hdrFlat02.md) | inherited class with overloads |

# final insights

something to think about for further work

## result

some incredible results here

## outlook

where to go now
