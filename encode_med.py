#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_med.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20200419
# CVS-ID:       $Id: encode_med.py,v 1.2 2020/04/19 17:59:38 wal Exp $
# Copyright:    (c) Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

command_lines = [
    "-u -i -f -n BtnGn00       v_img/baseGn00b.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnGn01       v_img/baseGn01b.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnGn02       v_img/baseGn02b.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnGn03       v_img/baseGn03b.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnGn04       v_img/baseGn04b.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnGn05       v_img/baseGn05b.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnGn06       v_img/baseGn06b.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnGn07       v_img/baseGn07b.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnGn08       v_img/baseGn08b.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnGn09       v_img/baseGn09b.ico            lindworm/ldmWidImgMed.py",
    
    "-a -u -f -n Gn00          v_img/baseGn00a.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n Gn01          v_img/baseGn01a.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n Gn02          v_img/baseGn02a.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n Gn03          v_img/baseGn03a.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n Gn04          v_img/baseGn04a.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n Gn05          v_img/baseGn05a.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n Gn06          v_img/baseGn06a.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n Gn07          v_img/baseGn07a.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n Gn08          v_img/baseGn08a.ico            lindworm/ldmWidImgMed.py",
    "-a -u -f -n Gn09          v_img/baseGn09a.ico            lindworm/ldmWidImgMed.py",
    
    "-a -u -f -n BtnSmlGn00     v_img/baseGn00_16.ico         lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnSmlGn01     v_img/baseGn01_16.ico         lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnSmlGn02     v_img/baseGn02_16.ico         lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnSmlGn03     v_img/baseGn03_16.ico         lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnSmlGn04     v_img/baseGn04_16.ico         lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnSmlGn05     v_img/baseGn05_16.ico         lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnSmlGn06     v_img/baseGn06_16.ico         lindworm/ldmWidImgMed.py",
    "-a -u -f -n BtnSmlGn07     v_img/baseGn07_16.ico         lindworm/ldmWidImgMed.py",
    #"-a -u -f -n BtnSmlGn08     v_img/baseGn08_16.ico         lindworm/ldmWidImgMed.py",
    #"-a -u -f -n BtnSmlGn09     v_img/baseGn09_16.ico         lindworm/ldmWidImgMed.py",
    
    "-a -u -f -n BtnSmlRd00     v_img/baseRd00_16.ico         lindworm/ldmWidImgMed.py",
    
    #"-a -u -f -n Rd00          v_img/baseRd00d.png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Rd01          v_img/baseRd01d.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Rd02          v_img/baseRd02h.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Rd03          v_img/baseRd03h.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Rd04          v_img/baseRd04h.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Rd05          v_img/baseRd05d.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Rd06          v_img/baseRd06d.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Rd07          v_img/baseRd07d.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Rd08          v_img/baseRd08d.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Rd09          v_img/baseRd09d.Png            lindworm/ldmWidImgMed.py",
    
    #"-a -u -f -n Bl00          v_img/baseBl00d.png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Bl01          v_img/baseBl01d.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Bl02          v_img/baseBl02h.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Bl03          v_img/baseBl03h.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Bl04          v_img/baseBl04h.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Bl05          v_img/baseBl05d.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Bl06          v_img/baseBl06d.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Bl07          v_img/baseBl07d.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Bl08          v_img/baseBl08d.Png            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Bl09          v_img/baseBl09d.Png            lindworm/ldmWidImgMed.py",

    #"-a -u -f -n Gy00          v_img/baseGy00d.ico            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Gy01          v_img/baseGy01d.ico            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Gy02          v_img/baseGy02h.ico            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Gy03          v_img/baseGy03h.ico            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Gy04          v_img/baseGy04h.ico            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Gy05          v_img/baseGy05d.ico            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Gy06          v_img/baseGy06d.ico            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Gy07          v_img/baseGy07d.ico            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Gy08          v_img/baseGy08d.ico            lindworm/ldmWidImgMed.py",
    #"-a -u -f -n Gy09          v_img/baseGy09d.ico            lindworm/ldmWidImgMed.py",
    
    ]
