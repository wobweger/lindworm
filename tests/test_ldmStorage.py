#----------------------------------------------------------------------------
# Name:         test_ldmStorage.py
# Purpose:      unit test of ldmStorageFolder.py
#
# Author:       Walter Obweger
#
# Created:      20200403
# CVS-ID:       $Id$
# Copyright:    (c) 2020 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

import sys
sys.path.insert(1,'.')

import unittest   # The test framework

import lindworm.logUtil as logUtil
import lindworm.ldmStorageFolder as UuT

try:
    import UuT_cfg
    isSkipLarge=UuT_cfg.isSkipLarge()
    isSkipGUI=UuT_cfg.isSkipGUI()
    iLgLv=UuT_cfg.getLogLevel()
except:
    import logging
    isSkipLarge=1
    isSkipGUI=1
    iLgLv=logging.ERROR

iLogInit=0

def initLog(sLogFN,sLogger=''):
    global iLogInit
    if iLogInit==0:
        logUtil.logInit(sLogFN,sLogger=sLogger,
                    iLevel=iLgLv)
        iLogInit=1

class TestBase(unittest.TestCase):
    def setUp(self):
        # +++++ beg:init logging
        initLog("./x_log/test_ldmStorage.log",sLogger=None)
        # ----- end:init logging
        # +++++ beg:initialize
        self.sSrcDN             ='./x_test/40_dat'
        self.sBldDN             ='./x_test/build'
        self.iShaMB             =1
        self.sBldFN             =None
        self.sCfgFN             ='./x_test/ldmStorageCfg.json'
        self.iModeRev           =0
        self.iVerbose           =10
        # ----- end:initialize
        # +++++ beg:construct object
        self.setUpObj()
        # ----- end:construct object
    def setUpObj(self):
        # +++++ beg:construct object
        pass
        # ----- end:construct object
    def tearDown(self):
        pass
    def testBase(self):
        iRet=1
        # +++++ beg:exec gather 
        #iRet=UuT.execMain(
        #                iModeRev=self.iModeRev,
        #                iVerbose=self.iVerbose)
        # ----- end:exec gather
        self.assertEqual(iRet,1)

class TestObj(TestBase):
    def setUpObj(self):
        # +++++ beg:construct object
        self.oUuT=UuT.ldmStorage(iModeRev=self.iModeRev,
                        sLogger='UuT',iLv=0,
                        iVerbose=self.iVerbose)
        # ----- end:construct object
    def tearDown(self):
        pass
    def testBase(self):
        # +++++ beg:exec gather 
        iRet=1
        self.oUuT.logDbg('beg:OnBldDN debug')
        self.oUuT.logDbg('beg:OnBldDN debug iRet:%d',iRet)
        self.oUuT.log(0,'beg:OnBldDN debug')
        self.oUuT.log(1,'beg:OnBldDN information')
        self.oUuT.log(2,'beg:OnBldDN warning')
        self.oUuT.log(3,'beg:OnBldDN error')
        self.oUuT.log(4,'beg:OnBldDN critical')
        self.oUuT.log(5,'beg:OnBldDN else')
        # ----- end:exec gather
        self.assertEqual(iRet,1)

if __name__ == '__main__':
    unittest.main()
