#----------------------------------------------------------------------------
# Name:         test_ldmStorageFolder.py
# Purpose:      unit test of ldmStorageFolder.py
#
# Author:       Walter Obweger
#
# Created:      20200323
# CVS-ID:       $Id$
# Copyright:    (c) 2020 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

import sys
sys.path.insert(1,'.')

import unittest   # The test framework

import lindworm.logUtil as logUtil
import lindworm.ldmStorageFolder as UuT

try:
    import UuT_cfg
    isSkipLarge=UuT_cfg.isSkipLarge()
    isSkipGUI=UuT_cfg.isSkipGUI()
    iLgLv=UuT_cfg.getLogLevel()
except:
    import logging
    isSkipLarge=1
    isSkipGUI=1
    iLgLv=logging.ERROR


iLogInit=0

def initLog(sLogFN,sLogger=''):
    global iLogInit
    #print('sLogFN:%s'%(sLogFN))
    if iLogInit==0:
        logUtil.logInit(sLogFN,sLogger=sLogger,
                    iLevel=iLgLv)
        iLogInit=1

class TestBase(unittest.TestCase):
    def setUp(self):
        # +++++ beg:init logging
        initLog("./x_log/test_ldmStorageFolder.log",sLogger=None)
        # ----- end:init logging
        # +++++ beg:initialize
        self.sSrcDN             ='./x_test/40_dat'
        self.sBldDN             ='./x_test/build'
        self.iShaMB             =1
        self.sBldFN             ='fdrBld.json'
        self.sCfgFN             ='./x_test/ldmStorageFolderCfg.json'
        self.iVerbose           =10
        # ----- end:initialize
        # +++++ beg:construct object
        # ----- end:construct object
    def tearDown(self):
        pass
    def test_buildDN(self):
        # +++++ beg:exec gather 
        iRet=UuT.execMain(sSrcDN=self.sSrcDN,
                                sBldFN=self.sBldFN,
                                sBldDN=self.sBldDN,
                                sCfgFN=self.sCfgFN,
                                #iShaMB=self.iShaMB,
                                iVerbose=self.iVerbose)
        # ----- end:exec gather
        self.assertEqual(iRet,2)

class TestDft(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
    def testMain(self):
        if isSkipLarge==False:
            # +++++ beg:exec gather 
            iRet=UuT.main(['--log',None])
            # ----- end:exec gather
            self.assertEqual(iRet,1)
        else:
            logUtil.logDbg('testMain skip large test')

class TestBaseLoc(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sBldFN             =0
        # ----- end:initialize

class TestBaseUTC(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sBldFN             =1
        # ----- end:initialize

class TestBaseOut(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sBldFN             ='TestBaseOut'
        # ----- end:initialize

class TestBaseOutJson(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sBldFN             ='TestBaseOut.json'
        # ----- end:initialize

class TestBase40(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sSrcDN             ='./x_test/40_dat'
        self.sBldFN             ='TestBase.40_dat'
        # ----- end:initialize

class TestBase41(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sSrcDN             ='./x_test/41_dat'
        self.sBldFN             ='TestBase.41_dat'
        # ----- end:initialize

if __name__ == '__main__':
    unittest.main()
