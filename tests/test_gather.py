#----------------------------------------------------------------------------
# Name:         test_gather.py
# Purpose:      unit test of pyGatherMD.py
#
# Author:       Walter Obweger
#
# Created:      20191102
# CVS-ID:       $Id$
# Copyright:    (c) 2019 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

import sys
import logging
import logging.handlers
import unittest   # The test framework

import lindworm.pyGatherMD as pyGatherMD

try:
    import UuT_cfg
    isSkipLarge=UuT_cfg.isSkipLarge()
    isSkipGUI=UuT_cfg.isSkipGUI()
    iLgLv=UuT_cfg.getLogLevel()
except:
    import logging
    isSkipLarge=1
    isSkipGUI=1
    iLgLv=logging.ERROR

iLogInit=0

def initLog(sLogFN):
    global iLogInit
    print('sLogFN:%s'%(sLogFN))
    if iLogInit==0:
        lngFileHandler=logging.handlers.RotatingFileHandler(sLogFN,backupCount=9)
        lngFileHandler.setLevel(iLgLv)
        formatter=logging.Formatter('%(asctime)s|%(name)-8s|%(levelname)-8s|%(message)s')
        lngFileHandler.setFormatter(formatter)
        logging.getLogger('').setLevel(iLgLv)
        logging.getLogger('').addHandler(lngFileHandler)
        lngFileHandler.doRollover()
        iLogInit=1

def newSha():
    """python 3 change fix
    """
    if sys.version_info  >= (2,6,0):
        import hashlib
        return hashlib.sha1()
    else:
        import sha
        return sha.new()

class TestBase(unittest.TestCase):
    def setUp(self):
        # +++++ beg:init logging
        initLog("./x_log/test_gather.log")
        # ----- end:init logging
        # +++++ beg:initialize
        self.sInFN              ='./x_test/README.md'
        self.sBldDN             ='./x_test/build'
        self.sOtFN              =None
        self.sCfgFN             ='./x_test/pyGatherMDCfg.json'
        self.iVerbose           =10
        # ----- end:initialize
        # +++++ beg:check result SHA
        self.sResFN='./x_test/build/README.md'
        self.sResSha='0fd79e5e40d7a7c95fbec3e71c42e03c66f15a29'
        # ----- end:check result SHA
        # +++++ beg:construct object
        # ----- end:construct object
    def getShaFN(self,sFN):
        with open(sFN,'r') as oFile:
            oSha=newSha()
            oSha.update(oFile.read().encode('utf-8','ignore'))
            return oSha.hexdigest()
        return '?err?'
    def tearDown(self):
        pass
    def test_buildDN(self):
        # +++++ beg:exec gather 
        iRet=pyGatherMD.execMain(sInFN=self.sInFN,
                                sOtFN=self.sOtFN,
                                sBldDN=self.sBldDN,
                                sCfgFN=self.sCfgFN,
                                iVerbose=self.iVerbose)
        # ----- end:exec gather
        self.assertEqual(iRet,1)
        # +++++ beg:check result SHA
        if self.sResFN is not None:
            if self.sResSha is not None:
                self.assertEqual(self.getShaFN(self.sResFN),self.sResSha)
        # ----- end:check result SHA

class TestAbort(TestBase):
    def test_abortOverwrite(self,iVerbose=0):
        # +++++ beg:exec gather 
        iRet=pyGatherMD.execMain(sInFN=self.sInFN,
                                sOtFN=None,
                                sBldDN=None,
                                sCfgFN=self.sCfgFN,
                                iVerbose=iVerbose)
        # ----- end:exec gather
        self.assertEqual(iRet,-10)
    def test_changedOut(self,iVerbose=0):
        # +++++ beg:exec gather 
        iRet=pyGatherMD.execMain(sInFN=self.sInFN,
                                sOtFN='./x_test/README_out.md',
                                sBldDN=None,
                                sCfgFN=self.sCfgFN,
                                iVerbose=iVerbose)
        # ----- end:exec gather
        self.assertEqual(iRet,1)
        # +++++ beg:check result SHA
        self.sResFN='./x_test/README_out.md'
        if self.sResFN is not None:
            self.assertEqual(self.getShaFN(self.sResFN),self.sResSha)
        # ----- end:check result SHA

class TestImageFlat(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sInFN              ='./x_test/imgFlat00.md'
        #self.sBldDN             ='./build'
        #self.sOtFN              =None
        #self.sCfgFN             ='pyGatherMDCfg.xml'
        #self.iVerbose           =10
        # ----- end:initialize
        # +++++ beg:check result SHA
        self.sResFN='./x_test/build/imgFlat00.md'
        self.sResSha='ecbc2ae312355da8a9f9b1bd05bb849a1387fca5'
        # ----- end:check result SHA
        # +++++ beg:construct object
        # ----- end:construct object

class TestNoRel(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sInFN              ='./x_test/imgNoRel.md'
        self.sBldDN             ='./x_test/build'
        self.sOtFN              ='result.md'
        #self.sCfgFN             ='pyGatherMDCfg.xml'
        self.iVerbose           =20
        # ----- end:initialize
        # +++++ beg:check result SHA
        self.sResFN='./x_test/build/result.md'
        self.sResSha='20f12bb735c3e48d90f1cd51369bb555e7fe6fe2'
        # ----- end:check result SHA
        # +++++ beg:construct object
        # ----- end:construct object

class TestHdrHier02(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sInFN              ='./x_test/hdrHier02.md'
        self.sBldDN             ='./x_test/build'
        self.sOtFN              ='result.md'
        #self.sCfgFN             ='pyGatherMDCfg.xml'
        self.iVerbose           =20
        # ----- end:initialize
        # +++++ beg:check result SHA
        self.sResFN='./x_test/build/result.md'
        self.sResSha='c85fc99ae534d5c4667c1c921a725fedb71db4b8'
        # ----- end:check result SHA
        # +++++ beg:construct object
        # ----- end:construct object

class TestHdrHier03(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sInFN              ='./x_test/hdrHier03.md'
        self.sBldDN             ='./x_test/build'
        self.sOtFN              ='result.md'
        #self.sCfgFN             ='pyGatherMDCfg.xml'
        self.iVerbose           =20
        # ----- end:initialize
        # +++++ beg:check result SHA
        self.sResFN='./x_test/build/result.md'
        self.sResSha='0399f3d4674f57f8f8b7b3ae1678257fda127a0e'
        # ----- end:check result SHA
        # +++++ beg:construct object
        # ----- end:construct object

class TestHdrHier04(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sInFN              ='./x_test/hdrHier04.md'
        self.sBldDN             ='./x_test/build'
        self.sOtFN              ='result.md'
        #self.sCfgFN             ='pyGatherMDCfg.xml'
        self.iVerbose           =20
        # ----- end:initialize
        # +++++ beg:check result SHA
        self.sResFN='./x_test/build/result.md'
        self.sResSha='c20d3434f6b3a903cb7b89e1ad03ef36c59ae158'
        # ----- end:check result SHA
        # +++++ beg:construct object
        # ----- end:construct object

class TestHdrHier05(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sInFN              ='./x_test/hdrHier05.md'
        self.sBldDN             ='./x_test/build'
        self.sOtFN              ='result.md'
        #self.sCfgFN             ='pyGatherMDCfg.xml'
        self.iVerbose           =20
        # ----- end:initialize
        # +++++ beg:check result SHA
        self.sResFN='./x_test/build/result.md'
        self.sResSha='896b5fcc9bd33e5921842cc0ee0ec715c5b23ffc'
        # ----- end:check result SHA
        # +++++ beg:construct object
        # ----- end:construct object

class TestLindWorm(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sInFN              ='README.md'
        self.sBldDN             ='./x_test/build'
        self.sOtFN              =None
        #self.sCfgFN             ='pyGatherMDCfg.xml'
        #self.iVerbose           =10
        # ----- end:initialize
        # +++++ beg:check result SHA
        if isSkipGUI==False:
            self.sResFN='./x_test/build/README.md'
            self.sResSha='f6b46f2ab4b5b51d426406e41e9edfdc3124b073'
        else:
            # skip this on pipeline
            self.sResFN=None
            self.sResSha=None
        # ----- end:check result SHA
        # +++++ beg:construct object
        # ----- end:construct object

if __name__ == '__main__':
    unittest.main()
