#----------------------------------------------------------------------------
# Name:         test_ldmViewFrm.py
# Purpose:      unit test of ldmStorageFolderViewFrm.py
#
# Author:       Walter Obweger
#
# Created:      20200404
# CVS-ID:       $Id$
# Copyright:    (c) 2020 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

import sys
sys.path.insert(1,'.')

import unittest   # The test framework

import lindworm.logUtil as logUtil
import lindworm.ldmStorageFolderViewFrm as UuT

from test_ldmStorageFolderViewFrm import TestBase

try:
    import UuT_cfg as UuT_cfg
    isSkipLarge=UuT_cfg.isSkipLarge()
    isSkipGUI=UuT_cfg.isSkipGUI()
    iLgLv=UuT_cfg.getLogLevel()
except:
    import logging
    isSkipLarge=1
    isSkipGUI=1
    iLgLv=logging.ERROR


iLogInit=0

class TestWroClone(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        #self.sSrcDN             ='c:/dat/BldFdr'
        self.sBldFN             ='c:/dat/BldFdr/wro2020_clone.json'
        # ----- end:initialize

if __name__ == '__main__':
    unittest.main()
