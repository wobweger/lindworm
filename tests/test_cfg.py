#----------------------------------------------------------------------------
# Name:         test_cfg.py
# Purpose:      unit test of pyGatherMD.py
#
# Author:       Walter Obweger
#
# Created:      20191223
# CVS-ID:       $Id$
# Copyright:    (c) 2019 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

import unittest   # The test framework

import lindworm.logUtil as logUtil
import lindworm.pyGatherMD as pyGatherMD

iLogInit=0

def initLog(sLogFN):
    global iLogInit
    print('sLogFN:%s'%(sLogFN))
    if iLogInit==0:
        logUtil.logInit(sLogFN)
        iLogInit=1

class TestBase(unittest.TestCase):
    def setUp(self):
        # +++++ beg:init logging
        initLog("./x_log/test_cfg.log")
        # ----- end:init logging
        # +++++ beg:initialize
        self.sInFN              ='./x_test/README.md'
        self.sBldDN             ='./x_test/build'
        self.sOtFN              =None
        self.sCfgFN             ='./x_test/pyGatherMDCfg.json'
        self.iVerbose           =10
        # ----- end:initialize
        # +++++ beg:construct object
        # ----- end:construct object
    def tearDown(self):
        pass
    def test_buildDN(self):
        # +++++ beg:exec gather 
        iRet=pyGatherMD.execMain(sInFN=self.sInFN,
                                sOtFN=self.sOtFN,
                                sBldDN=self.sBldDN,
                                sCfgFN=self.sCfgFN,
                                iVerbose=self.iVerbose)
        # ----- end:exec gather
        self.assertEqual(iRet,1)

if __name__ == '__main__':
    unittest.main()
