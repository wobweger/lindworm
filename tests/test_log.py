#----------------------------------------------------------------------------
# Name:         test_log.py
# Purpose:      unit test of logUtil.py
#
# Author:       Walter Obweger
#
# Created:      20191223
# CVS-ID:       $Id$
# Copyright:    (c) 2019 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

import sys
sys.path.insert(1,'.')

import unittest   # The test framework

import lindworm.logUtil as logUtil

iLogInit=0

def initLog(sLogFN):
    global iLogInit
    print('sLogFN:%s'%(sLogFN))
    if iLogInit==0:
        logUtil.logInit(sLogFN)
        iLogInit=1

class TestBase(unittest.TestCase):
    def setUp(self):
        # +++++ beg:init logging
        initLog("./x_log/test_cfg.log")
        # ----- end:init logging
        # +++++ beg:initialize
        self.sInFN              ='./x_test/README.md'
        self.sBldDN             ='./x_test/build'
        self.sOtFN              =None
        self.sCfgFN             ='./x_test/pyGatherMDCfg.json'
        self.iVerbose           =10
        # ----- end:initialize
        # +++++ beg:construct object
        # ----- end:construct object
    def tearDown(self):
        pass
    def test_logDbg(self):
        # +++++ beg:log
        logUtil.logDbg('message 0')
        logUtil.logDbg('message %d'%(1))
        logUtil.logDbg('message %d %-10s',2,'args')
        # ----- end:log
    def test_logInf(self):
        # +++++ beg:log
        logUtil.logInf('message 0')
        logUtil.logInf('message %d'%(1))
        logUtil.logInf('message %d %-10s',2,'args')
        # ----- end:log
    def test_logWrn(self):
        # +++++ beg:log
        logUtil.logWrn('message 0')
        logUtil.logWrn('message %d'%(1))
        logUtil.logWrn('message %d %-10s',2,'args')
        # ----- end:log
    def test_logErr(self):
        # +++++ beg:log
        logUtil.logErr('message 0')
        logUtil.logErr('message %d'%(1))
        logUtil.logErr('message %d %-10s',2,'args')
        # ----- end:log
    def test_logCri(self):
        # +++++ beg:log
        logUtil.logCri('message 0')
        logUtil.logCri('message %d'%(1))
        logUtil.logCri('message %d %-10s',2,'args')
        # ----- end:log

class TestUtilLog(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        self.sLogger="TestUtilLog"
        self.iLv=0
        self.iLevel=None
        self.sOrg=""
        self.iIndent=2
        self.oLog=None
    def getUtilLog(self,iNew=0):
        if iNew<1:
            if self.oLog is not None:
                return self.oLog
        self.oLog=logUtil.ldmUtilLog(sLogger=self.sLogger,
                    iLv=self.iLv,
                    iLevel=self.iLevel,
                    iVerbose=self.iVerbose,
                    sOrg=self.sOrg,
                    iIndent=self.iIndent)
        return self.oLog
    def test_log(self):
        self.getUtilLog()
        # +++++ beg:log
        self.oLog.log(0,'dbg message 0')
        self.oLog.log(1,'inf message %d'%(1))
        self.oLog.log(2,'wrn message %d %-10s',2,'args')
        self.oLog.log(3,'err message %d %-10s',3,'args')
        self.oLog.log(4,'cri message %d %-10s',4,'args')
        # ----- end:log
    def test_logDbg(self):
        self.getUtilLog()
        # +++++ beg:log
        self.oLog.logDbg('message 0')
        self.oLog.logDbg('message %d'%(1))
        self.oLog.logDbg('message %d %-10s',2,'args')
        # ----- end:log
    def test_logInf(self):
        self.getUtilLog()
        # +++++ beg:log
        self.oLog.logInf('message 0')
        self.oLog.logInf('message %d'%(1))
        self.oLog.logInf('message %d %-10s',2,'args')
        # ----- end:log
    def test_logWrn(self):
        self.getUtilLog()
        # +++++ beg:log
        self.oLog.logWrn('message 0')
        self.oLog.logWrn('message %d'%(1))
        self.oLog.logWrn('message %d %-10s',2,'args')
        # ----- end:log
    def test_logErr(self):
        self.getUtilLog()
        # +++++ beg:log
        self.oLog.logErr('message 0')
        self.oLog.logErr('message %d'%(1))
        self.oLog.logErr('message %d %-10s',2,'args')
        # ----- end:log
    def test_logCri(self):
        self.getUtilLog()
        # +++++ beg:log
        self.oLog.logCri('message 0')
        self.oLog.logCri('message %d'%(1))
        self.oLog.logCri('message %d %-10s',2,'args')
        # ----- end:log

class TestIndent(TestUtilLog):
    def setUp(self):
        TestUtilLog.setUp(self)
        self.sLogger="TestIndent"
    def test_incDepth(self):
        self.getUtilLog()
        for iOfs in range(0,5):
            # +++++ beg:log
            self.oLog.incDepth()
            self.oLog.log(0,'dbg message 0')
            self.oLog.log(1,'inf message %d'%(1))
            self.oLog.log(2,'wrn message %d %-10s',2,'args')
            self.oLog.log(3,'err message %d %-10s',3,'args')
            self.oLog.log(4,'cri message %d %-10s',4,'args')
            # ----- end:log
    def test_decDepth(self):
        self.getUtilLog()
        for iOfs in range(0,5):
            # +++++ beg:log
            self.oLog.incDepth()
            self.oLog.log(0,'dbg message iOfs:%03d 0',iOfs)
            self.oLog.log(1,'inf message iOfs:%03d %d'%(iOfs,1))
            self.oLog.log(2,'wrn message iOfs:%03d %d %-10s',iOfs,2,'args')
            self.oLog.log(3,'err message iOfs:%03d %d %-10s',iOfs,3,'args')
            self.oLog.log(4,'cri message iOfs:%03d %d %-10s',iOfs,4,'args')
            # ----- end:log
        for iOfs in range(0,5):
            # +++++ beg:log
            self.oLog.decDepth()
            self.oLog.log(0,'dbg message iOfs:%03d 0',iOfs)
            self.oLog.log(1,'inf message iOfs:%03d %d'%(iOfs,1))
            self.oLog.log(2,'wrn message iOfs:%03d %d %-10s',iOfs,2,'args')
            self.oLog.log(3,'err message iOfs:%03d %d %-10s',iOfs,3,'args')
            self.oLog.log(4,'cri message iOfs:%03d %d %-10s',iOfs,4,'args')
            # ----- end:log
        for iOfs in range(2):
            self.oLog.decDepth()
            self.oLog.log(0,'dbg message iOfs:%03d 0',iOfs)

class TestUtilLogInf(TestUtilLog):
    def setUp(self):
        TestUtilLog.setUp(self)
        self.sLogger=self.__class__.__name__
        self.iLv=1

class TestUtilLogWrn(TestUtilLog):
    def setUp(self):
        TestUtilLog.setUp(self)
        self.sLogger=self.__class__.__name__
        self.iLv=2

class TestUtilLogErr(TestUtilLog):
    def setUp(self):
        TestUtilLog.setUp(self)
        self.sLogger=self.__class__.__name__
        self.iLv=3

class TestUtilLogCri(TestUtilLog):
    def setUp(self):
        TestUtilLog.setUp(self)
        self.sLogger=self.__class__.__name__
        self.iLv=4

class TestVerbose(TestUtilLog):
    def setUp(self):
        TestUtilLog.setUp(self)
        self.sLogger=self.__class__.__name__
        self.iVerbose=10
    def test_log(self):
        self.getUtilLog()
        # +++++ beg:log
        if self.oLog.GetVerbose(15):
            self.oLog.log(0,'dbg message 0')
        if self.oLog.GetVerbose(10):
            self.oLog.log(1,'inf message %d'%(1))
        if self.oLog.GetVerbose(5):
            self.oLog.log(2,'wrn message %d %-10s',2,'args')
        if self.oLog.GetVerbose(15):
            self.oLog.log(3,'err message %d %-10s',3,'args')
        if self.oLog.GetVerbose(15):
            self.oLog.log(4,'cri message %d %-10s',4,'args')
        # ----- end:log
    def test_logDbg(self):
        self.getUtilLog()
        # +++++ beg:log
        if self.oLog.GetVerboseDbg(15):
            self.oLog.logDbg('message 0')
        if self.oLog.GetVerboseDbg(10):
            self.oLog.logDbg('message %d'%(1))
        if self.oLog.GetVerboseDbg(5):
            self.oLog.logDbg('message %d %-10s',2,'args')
        # ----- end:log
    def test_logInf(self):
        self.getUtilLog()
        # +++++ beg:log
        if self.oLog.GetVerboseDbg(15):
            self.oLog.logInf('message 0')
        if self.oLog.GetVerboseDbg(10):
            self.oLog.logInf('message %d'%(1))
        if self.oLog.GetVerboseDbg(5):
            self.oLog.logInf('message %d %-10s',2,'args')
        # ----- end:log
    def test_logWrn(self):
        self.getUtilLog()
        # +++++ beg:log
        if self.oLog.GetVerboseDbg(15):
            self.oLog.logWrn('message 0')
        if self.oLog.GetVerboseDbg(10):
            self.oLog.logWrn('message %d'%(1))
        if self.oLog.GetVerboseDbg(5):
            self.oLog.logWrn('message %d %-10s',2,'args')
        # ----- end:log
    def test_logErr(self):
        self.getUtilLog()
        # +++++ beg:log
        if self.oLog.GetVerboseDbg(15):
            self.oLog.logErr('message 0')
        if self.oLog.GetVerboseDbg(10):
            self.oLog.logErr('message %d'%(1))
        if self.oLog.GetVerboseDbg(5):
            self.oLog.logErr('message %d %-10s',2,'args')
        # ----- end:log
    def test_logCri(self):
        self.getUtilLog()
        # +++++ beg:log
        if self.oLog.GetVerboseDbg(15):
            self.oLog.logCri('message 0')
        if self.oLog.GetVerboseDbg(10):
            self.oLog.logCri('message %d'%(1))
        if self.oLog.GetVerboseDbg(5):
            self.oLog.logCri('message %d %-10s',2,'args')
        # ----- end:log
    

if __name__ == '__main__':
    unittest.main()
