# test cases

summary

| test case | comment |
|---        | --- |
|TestBase   ||
|[>.test_buildDN](#TestBase.test_buildDN)||
|TestAbort  |
|[>.test_buildDN](#TestBase.test_buildDN)||
|[>.test_abortOverwrite](#TestAbort.test_abortOverwrite)| abort to avoid overwriting input file |
|[>.test_changedOut](#TestAbort.test_changedOut)| |
|TestImageFlat||
|[>.test_buildDNUp](#TestImageFlat.test_buildDNUp)| |
|TestNoRel||
|[>.test_buildDNUp](#TestNoRel.test_buildDNUp)| |
|TestHdrHier02||
|[>.test_buildDNUp](#TestHdrHier02.test_buildDNUp)| |
|TestHdrHier03||
|[>.test_buildDNUp](#TestHdrHier03.test_buildDNUp)| |
|TestHdrHier04||
|[>.test_buildDNUp](#TestHdrHier04.test_buildDNUp)| |
|TestHdrHier05||
|[>.test_buildDNUp](#TestHdrHier05.test_buildDNUp)| |
|TestLindWorm||
|[>.test_buildDNUp](#TestLindWorm.test_buildDNUp)| |

## TestBase

base unit test case object to be inherited by all following

### TestBase.setUp{#TestBase.setUp}

use method `setUp` to change arguments passed to entry point

default values

|arguments | value |
|---:   | --- |
|sInFN  |'./x_test/README.md'
|sOtFN  |None|
|sBldDN |'./x_test/build'
|sCfgFN |'./x_test/pyGatherMDCfg.xml'
|iVerbose|10|

### TestBase.test_buildDN{#TestBase.test_buildDN}

build result into folder specified by `sBldDN`  
base unit test case method to be present at all following test cases
use method `setUp` to change arguments passed to entry point

entry point:`pyGatherMD.execMain`  

|arguments | value |
|---:   | --- |
|sInFN  |'./x_test/README.md'
|sOtFN  |None|
|sBldDN |'./x_test/build'
|sCfgFN |'./x_test/pyGatherMDCfg.xml'
|iVerbose|10|
|-- |**result**|
|iRet|1|
|FN|'./x_test/build/README.md'
|sha1||

## TestAbort

test abort mechanism implemented, protect input file and ensure output possible

### TestAbort.setUp{#TestAbort.setUp}

+ [TestBase.setUp](#TestBase.setUp): unchanged

### TestAbort.test_buildDN{#TestAbort.settest_buildDNUp}

+ [TestBase.test_buildDN](#TestBase.test_buildDN): unchanged

### TestAbort.test_abortOverwrite{#TestAbort.test_abortOverwrite}

abort to protect input file from being overwritten

entry point:`pyGatherMD.execMain`

+ iRet=-10

|arguments | value |
|---:   | --- |
|sInFN  |'./x_test/README.md'
|sOtFN  |None|
|sBldDN |None|
|sCfgFN |'./x_test/pyGatherMDCfg.xml'
|iVerbose|10|
|-- |**result**|
|iRet|-10|

### TestAbort.test_changedOut{#TestAbort.test_changedOut}

entry point:`pyGatherMD.execMain`

|arguments | value |
|---:   | --- |
|sInFN  |'./x_test/README.md'
|sOtFN  |'./x_test/README_out.md'|
|sBldDN |None|
|sCfgFN |'./x_test/pyGatherMDCfg.xml'
|iVerbose|10|
|-- |**result**|
|iRet|1|

## TestImageFlat

### TestImageFlat.test_buildDN{#TestImageFlat.test_buildDNUp}

+ [TestBase.test_buildDN](#TestBase.test_buildDN): unchanged

|arguments | value |
|---:   | --- |
|sInFN  |'./x_test/imgFlat00.md'
|sOtFN  |None|
|sBldDN |'./x_test/build'
|sCfgFN |'./x_test/pyGatherMDCfg.xml'
|iVerbose|10|
|-- |**result**|
|iRet|1|
|FN|'./x_test/build/imgFlat00.md'
|sha1|ecbc2ae312355da8a9f9b1bd05bb849a1387fca5|

## TestNoRel

### TestNoRel.test_buildDN{#TestNoRel.test_buildDNUp}

+ [TestBase.test_buildDN](#TestBase.test_buildDN): unchanged

|arguments | value |
|---:   | --- |
|sInFN  |'./x_test/imgFlat00.md'
|sOtFN  |None|
|sBldDN |'./x_test/build'
|sCfgFN |'./x_test/pyGatherMDCfg.xml'
|iVerbose|10|
|-- |**result**|
|iRet|1|
|FN|'./x_test/build/imgFlat00.md'
|sha1|20f12bb735c3e48d90f1cd51369bb555e7fe6fe2|

## TestHdrHier02

include 3 sub files

### TestHdrHier02.test_buildDN{#TestHdrHier02.test_buildDNUp}

+ [TestBase.test_buildDN](#TestBase.test_buildDN): unchanged

|arguments | value |
|---:   | --- |
|sInFN  |'./x_test/hdrHier02.md'
|sOtFN  |None|
|sBldDN |'./x_test/build'
|sCfgFN |'./x_test/pyGatherMDCfg.xml'
|iVerbose|10|
|-- |**result**|
|iRet|1|
|FN|'./x_test/build/result.md'
|sha1|fe131ef03afc568337708d13a6daa8f0b7bbde46|

## TestHdrHier03

include 3 sub files as itemized list

### TestHdrHier03.test_buildDN{#TestHdrHier03.test_buildDNUp}

+ [TestBase.test_buildDN](#TestBase.test_buildDN): unchanged

|arguments | value |
|---:   | --- |
|sInFN  |'./x_test/hdrHier03.md'
|sOtFN  |None|
|sBldDN |'./x_test/build'
|sCfgFN |'./x_test/pyGatherMDCfg.xml'
|iVerbose|10|
|-- |**result**|
|iRet|1|
|FN|'./x_test/build/result.md'
|sha1|0f15a72557871a7a2e3f1b349805b8e6a2430985|

## TestHdrHier04

include 3 sub files in a table

### TestHdrHier04.test_buildDN{#TestHdrHier04.test_buildDNUp}

+ [TestBase.test_buildDN](#TestBase.test_buildDN): unchanged

|arguments | value |
|---:   | --- |
|sInFN  |'./x_test/hdrHier04.md'
|sOtFN  |None|
|sBldDN |'./x_test/build'
|sCfgFN |'./x_test/pyGatherMDCfg.xml'
|iVerbose|10|
|-- |**result**|
|iRet|1|
|FN|'./x_test/build/result.md'
|sha1|3f60d0eb6c9090626720921debade926e2e09778|

## TestHdrHier05

include 3 sub files multiple times (itemize list and table)

### TestHdrHier05.test_buildDN{#TestHdrHier05.test_buildDNUp}

+ [TestBase.test_buildDN](#TestBase.test_buildDN): unchanged

|arguments | value |
|---:   | --- |
|sInFN  |'./x_test/hdrHier02.md'
|sOtFN  |None|
|sBldDN |'./x_test/build'
|sCfgFN |'./x_test/pyGatherMDCfg.xml'
|iVerbose|10|
|-- |**result**|
|iRet|1|
|FN|'./x_test/build/result.md'
|sha1|3b99f1ddfde6d7936636b572e7036ef3de8e0fb7|

## TestLindWorm

run test on repository documentation

### TestTestLindWorm.test_buildDN{#TestLindWorm.test_buildDNUp}

+ [TestBase.test_buildDN](#TestBase.test_buildDN): unchanged

|arguments | value |
|---:   | --- |
|sInFN  |'README.md'
|sOtFN  |None|
|sBldDN |'./x_test/build'
|sCfgFN |'./x_test/pyGatherMDCfg.xml'
|iVerbose|10|
|-- |**result**|
|iRet|1|
|FN|'./x_test/build/README.md'
|sha1|418c1126c72c102571d912e144aed20fdb208093|
