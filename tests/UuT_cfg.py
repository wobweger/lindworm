#----------------------------------------------------------------------------
# Name:         UuT_cfg.py
# Purpose:      unit under test configuration
#               support local and CI pipeline processing
#               local more test are performed, large and GUI
#               CI pipeline uses different logging level and verbose level
#               
# Author:       Walter Obweger
#
# Created:      20220620
# CVS-ID:       $Id$
# Copyright:    (c) 2022 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

import os
import logging

iVerbose=0

DICT_UuT_CFG={
    'UuT_CFG_SKIP_LARGE'    :{'dft':'false'  ,'chk':['true','TRUE','1','1']},
    'UuT_CFG_SKIP_GUI'      :{'dft':'true'  ,'chk':['true','TRUE','1','1']},
    'UuT_CFG_LOG_Level'     :{'dft':'dbg'    ,},
    }

if iVerbose>0:
    try:
        for sUuT,dUuT in DICT_UuT_CFG.items():
            print('UuT_cfg for environment variable %s ...'%(sUuT))
            print('   ','value:',os.environ.get(sUuT,'???'))
    except:
        print('UuT_cfg','exception')

def isCfg(sKey='',iRetTB=0):
    try:
        if sKey in DICT_UuT_CFG:
            dUuT=DICT_UuT_CFG[sKey]
            sEnvVar=os.environ.get(sKey,dUuT['dft'])
            if sEnvVar in dUuT['chk']:
                return 1
            else:
                return 0
    except:
        return iRetTB
def isSkipLarge():
    return isCfg('UuT_CFG_SKIP_LARGE',iRetTB=0)

def isSkipGUI():
    return isCfg('UuT_CFG_SKIP_GUI',iRetTB=0)

def getLogLevel():
    try:
        sEnvVar=os.environ.get('UuT_CFG_LOG_Level','dbg')
        if sEnvVar in ['dbg','debug','DEBUG','10']:
            return logging.DEBUG
        elif sEnvVar in ['inf','info','INFO','20']:
            return logging.INFO
        elif sEnvVar in ['wrn','warn','WARN','30']:
            return logging.WARN
        elif sEnvVar in ['err','error','ERROR','40']:
            return logging.ERROR
        elif sEnvVar in ['ctl','critical','CRITICAL','50']:
            return logging.CRITICAL
        else:
            return logging.ERROR
    except:
        return logging.DEBUG
