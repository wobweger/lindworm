#----------------------------------------------------------------------------
# Name:         test_ldmWidApp.py
# Purpose:      unit test of ldmWidApp.py
#
# Author:       Walter Obweger
#
# Created:      20200404
# CVS-ID:       $Id$
# Copyright:    (c) 2020 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

import sys
sys.path.insert(1,'.')

import unittest   # The test framework

import lindworm.logUtil as logUtil
import lindworm.ldmWidApp as UuT

try:
    import UuT_cfg
    isSkipLarge=UuT_cfg.isSkipLarge()
    isSkipGUI=UuT_cfg.isSkipGUI()
    iLgLv=UuT_cfg.getLogLevel()
except:
    import logging
    isSkipLarge=1
    isSkipGUI=1
    iLgLv=logging.ERROR

if isSkipGUI==False:
    import tstFrmAuiCenter


iLogInit=0

def initLog(sLogFN,sLogger=''):
    global iLogInit
    #print('sLogFN:%s'%(sLogFN))
    if iLogInit==0:
        logUtil.logInit(sLogFN,sLogger=sLogger,
                    iLevel=iLgLv)
        iLogInit=1

class TestBase(unittest.TestCase):
    def setUp(self):
        # +++++ beg:init logging
        initLog("./x_log/test_ldmWidApp.log",sLogger=None)
        # ----- end:init logging
        # +++++ beg:initialize
        self.sCfgFN             ='./x_test/ldmWidAppCfg.json'
        self.iVerbose           =10
        # ----- end:initialize
        # +++++ beg:construct object
        # ----- end:construct object
    def tearDown(self):
        pass
    def testMain(self):
        if isSkipGUI==False:
            # +++++ beg:exec gather 
            iRet=UuT.main([
                '--log',    None,
                '--cfgFN',  self.sCfgFN,

                ])
            # ----- end:exec gather
            self.assertEqual(iRet,9)
        else:
            logUtil.logDbg('testMain skip GUI test')

class TestDft(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
    def testMain(self):
        if isSkipGUI==False:
            # +++++ beg:exec gather 
            iRet=UuT.main(['--log',None])
            # ----- end:exec gather
            self.assertEqual(iRet,9)
        else:
            logUtil.logDbg('testMain skip GUI test')

class TestCenterRow(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiCenterRows
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')
    def testMain(self):
        if isSkipGUI==False:
            # +++++ beg:exec gather 
            iRet=UuT.main([
                '--log',    None,
                '--cfgFN',  self.sCfgFN,

                ],
                self.clsFrm
                )
            # ----- end:exec gather
            self.assertEqual(iRet,9)
        else:
            logUtil.logDbg('testMain skip GUI test')

class TestCenterNoteBook(TestCenterRow):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiCenterNoteBook
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')

class TestCenterNoteBookList(TestCenterRow):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiCenterNoteBookList
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')

class TestCenterNoteBookThd(TestCenterRow):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiCenterNoteBookThd
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')

class TestCenterBottomTree(TestCenterRow):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiBottomTree
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')

class TestCenterBottomLog(TestCenterRow):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiBottomLog
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')

class TestTbrFile(TestCenterRow):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiTbrFile
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')

class TestCenterLeftSizer(TestCenterRow):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiLeftSizer
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')

class TestTbrDstRadio(TestCenterRow):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiTbrDstRadio
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')

class TestTbrDstCheck(TestCenterRow):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiTbrDstCheck
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')

class TestList(TestCenterRow):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiList
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')

class TestListCfgDst(TestList):
    def setUp(self):
        TestList.setUp(self)
        # +++++ beg:initialize
        self.sCfgFN             ='./x_test/ldmWidAppCfgDst.json'
        self.iVerbose           =10
        # ----- end:initialize

class TestListCfgTbrHid(TestList):
    def setUp(self):
        TestList.setUp(self)
        # +++++ beg:initialize
        self.sCfgFN             ='./x_test/ldmWidAppCfgTbrHid.json'
        self.iVerbose           =10
        # ----- end:initialize

class TestTree(TestCenterRow):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
        if isSkipGUI==False:
            self.clsFrm=tstFrmAuiCenter.tstFrmAuiTree
        else:
            self.clsFrm=None
            logUtil.logDbg('testMain skip GUI test')

if __name__ == '__main__':
    unittest.main()
