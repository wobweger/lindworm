#----------------------------------------------------------------------------
# Name:         tstFrmAuiCenter.py
# Purpose:      tstFrmAuiCenter.py
#               frame widget utilizing advanced user interface
# Author:       Walter Obweger
#
# Created:      20200405
# CVS-ID:       $Id$
# Copyright:    (c) 2020 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

from lindworm.ldmWidFrmAui import ldmWidFrmAui
from lindworm.ldmWidSizeRspWid import ldmWidSizeRspWid
from lindworm.ldmWidList import ldmWidList
from lindworm.ldmWidTree import ldmWidTree
from lindworm.ldmWidThd import ldmWidThd

import wx
import wx.aui

class tstFrmAuiCenterRows(ldmWidFrmAui):
    def __initPanCt__(self,**kwargs):
        try:
            self.oApi.Name("szRsp0")
            self.oApi.Caption("size responder 0")
            self.oApi.MinimizeButton(True)
            #oApi.MinimizeButton(False)
            self.oApi.CloseButton(False)
            oWid=ldmWidSizeRspWid(self.GetWid(), -1,mgr=self.mgrMain)
            self.mgrMain.AddPane(oWid,self.oApi)

            self.oApi.Name('szRsp1')
            self.oApi.Caption("size responder 1")
            self.oApi.Dockable(True)
            self.oApi.Row(0)
            oWid=ldmWidSizeRspWid(self.GetWid(), -1,mgr=self.mgrMain)
            self.mgrMain.AddPane(oWid,self.oApi)

            self.oApi.Name('trDef')
            self.oApi.Caption("tree 1")
            self.oApi.Dockable(True)
            self.oApi.Floatable(True)
            self.oApi.Row(1)
            oWid=ldmWidTree(parent=self.GetWid(),iLv=0,size=(200,120),sLogger='tr')
            self.mgrMain.AddPane(oWid.GetWid(),self.oApi)
        except:
            self.logTB()

class tstFrmAuiTbrFile(ldmWidFrmAui):
    def __initTbrFile__(self,**kwargs):
        try:
            oSz=self.tTbrBmpSz
            iStyle =wx.aui.AUI_TB_DEFAULT_STYLE
            #iStyle|=wx.aui.AUI_TB_OVERFLOW
            tbr = wx.aui.AuiToolBar(self.GetWid(),
                        -1,
                        wx.DefaultPosition,
                        size=oSz or wx.DefaultSize,
                        style=iStyle)
            #tbr.SetToolBitmapSize(oSz)
            
            bmp=wx.ArtProvider.GetBitmap(wx.ART_FILE_OPEN,size=oSz)
            w=tbr.AddTool(self.ID_FILE_OPEN,
                        label="Open",
                        bitmap=bmp,
                        kind=wx.ITEM_NORMAL,
                        short_help_string='open json file')
            #self.wid.Bind(wx.EVT_BUTTON,self.OnFileOpen,w)
            #self.wid.Bind(wx.EVT_MENU,self.OnFileOpen,w)
            self.BindEvent('mn',self.OnFileOpen,w)
            bmp=wx.ArtProvider.GetBitmap(wx.ART_FILE_SAVE,size=oSz)
            w=tbr.AddTool(self.ID_FILE_SAVE,
                        label="Save",
                        bitmap=bmp,
                        kind=wx.ITEM_NORMAL,
                        short_help_string='save json file')
            self.BindEvent('mn',self.OnFileSave,w)
            #tbr.Realize()
            #tbr.Refresh()
            #wx.aui.AuiPaneInfo().Name("tb1").Caption("Big Toolbar").
            #            ToolbarPane().Top()
            oApi=wx.aui.AuiPaneInfo().Name('tbrFile')
            oApi.Caption('file toolbar')
            oApi.ToolbarPane().Top()
            self.mgrMain.AddPane(tbr,oApi)
        except:
            self.logTB()

class tstFrmAuiTbrDstRadio(ldmWidFrmAui):
    def __initTbrDst__(self,**kwargs):
        try:
            oSz=self.tTbrBmpSz
            iStyle =wx.aui.AUI_TB_DEFAULT_STYLE
            iStyle|=wx.aui.AUI_TB_VERTICAL
            tbr = wx.aui.AuiToolBar(self.GetWid(),
                        -1,
                        wx.DefaultPosition,
                        size=oSz or wx.DefaultSize,
                        style=iStyle)
            #tbr.SetToolBitmapSize(oSz)
            
            bmp=wx.ArtProvider.GetBitmap(wx.ART_HARDDISK,size=oSz)
            w=tbr.AddTool(self.ID_DST+0,
                        label="00",
                        bitmap=bmp,
                        kind=wx.ITEM_RADIO,
                        short_help_string='destination 00')
            self.BindEvent('mn',self.OnDst00,w)
            #AUI_BUTTON_STATE_NORMAL
            #AUI_BUTTON_STATE_HOVER
            #AUI_BUTTON_STATE_PRESSED
            #AUI_BUTTON_STATE_DISABLED
            #AUI_BUTTON_STATE_HIDDEN
            w.SetState(wx.aui.AUI_BUTTON_STATE_CHECKED)
            #print(w.GetState())
            bmp=wx.ArtProvider.GetBitmap(wx.ART_HARDDISK,size=oSz)
            w=tbr.AddTool(self.ID_DST+1,
                        label="01",
                        bitmap=bmp,
                        kind=wx.ITEM_RADIO,
                        short_help_string='destination 01')
            self.BindEvent('mn',self.OnDst01,w)
            
            oApi=wx.aui.AuiPaneInfo().Name('tbrDst')
            oApi.Caption('destination toolbar')
            oApi.ToolbarPane().Right()
            oApi.GripperTop().TopDockable(False).BottomDockable(False)
            self.mgrMain.AddPane(tbr,oApi)
        except:
            self.logTB()
    def OnDst00(self,evt):
        try:
            o=evt.GetEventObject()
            iId=evt.GetId()
            self.logDbg('OnDst00 iId:%d 0x%x',iId,iId)
            w=o.FindTool(iId)
            iState=w.GetState()
            self.logDbg('OnDst00 iState:%d 0x%x',iState,iState)
        except:
            self.logTB()
    def OnDst01(self,evt):
        try:
            o=evt.GetEventObject()
            iId=evt.GetId()
            self.logDbg('OnDst01 iId:%d 0x%x',iId,iId)
            w=o.FindTool(iId)
            iState=w.GetState()
            self.logDbg('OnDst01 iState:%d 0x%x',iState,iState)
        except:
            self.logTB()

class tstFrmAuiTbrDstCheck(ldmWidFrmAui):
    def __initTbrDst__(self,**kwargs):
        try:
            oSz=wx.Size(16, 16)
            iStyle =wx.aui.AUI_TB_DEFAULT_STYLE
            iStyle|=wx.aui.AUI_TB_VERTICAL
            tbr = wx.aui.AuiToolBar(self.GetWid(),
                        -1,
                        wx.DefaultPosition,
                        size=oSz or wx.DefaultSize,
                        style=iStyle)
            #tbr.SetToolBitmapSize(oSz)
            
            bmp=wx.ArtProvider.GetBitmap(wx.ART_HARDDISK,size=oSz)
            w=tbr.AddTool(self.ID_DST+0,
                        label="00",
                        bitmap=bmp,
                        kind=wx.ITEM_CHECK,
                        short_help_string='destination 00')
            self.BindEvent('mn',self.OnDst00,w)
            #AUI_BUTTON_STATE_NORMAL
            #AUI_BUTTON_STATE_HOVER
            #AUI_BUTTON_STATE_PRESSED
            #AUI_BUTTON_STATE_DISABLED
            #AUI_BUTTON_STATE_HIDDEN
            w.SetState(wx.aui.AUI_BUTTON_STATE_CHECKED)
            #print(w.GetState())
            bmp=wx.ArtProvider.GetBitmap(wx.ART_HARDDISK,size=oSz)
            w=tbr.AddTool(self.ID_DST+1,
                        label="01",
                        bitmap=bmp,
                        kind=wx.ITEM_CHECK,
                        short_help_string='destination 01')
            self.BindEvent('mn',self.OnDst01,w)
            
            oApi=wx.aui.AuiPaneInfo().Name('tbrDst')
            oApi.Caption('destination toolbar')
            oApi.ToolbarPane().Right()
            oApi.GripperTop().TopDockable(False).BottomDockable(False)
            self.mgrMain.AddPane(tbr,oApi)
        except:
            self.logTB()
    def OnDst00(self,evt):
        try:
            o=evt.GetEventObject()
            iId=evt.GetId()
            self.logDbg('OnDst00 iId:%d 0x%x',iId,iId)
            w=o.FindTool(iId)
            iState=w.GetState()
            self.logDbg('OnDst00 iState:%d 0x%x',iState,iState)
        except:
            self.logTB()
    def OnDst01(self,evt):
        try:
            o=evt.GetEventObject()
            iId=evt.GetId()
            self.logDbg('OnDst01 iId:%d 0x%x',iId,iId)
            w=o.FindTool(iId)
            iState=w.GetState()
            self.logDbg('OnDst01 iState:%d 0x%x',iState,iState)
        except:
            self.logTB()

class tstFrmAuiCenterNoteBook(ldmWidFrmAui):
    def __initPanCt__(self,**kwargs):
        try:
            
            iStyle =wx.aui.AUI_NB_TOP | wx.aui.AUI_NB_TAB_SPLIT 
            iStyle|=wx.aui.AUI_NB_SCROLL_BUTTONS
            #iStyle|=wx.aui.AUI_NB_TAB_MOVE
            #iStyle|=wx.aui.AUI_NB_CLOSE_ON_ACTIVE_TAB
            #iStyle|=wx.aui.AUI_NB_MIDDLE_CLICK_CLOSE
            wNb=wx.aui.AuiNotebook(self.GetWid(), -1, 
                        (0,0),#wx.Point(client_size.x, client_size.y),
                        wx.Size(430, 200),
                        style=iStyle)
            #art=wx.aui.AuiDefaultTabArt()
            #art=wx.aui.AuiSimpleTabArt()
            #wNb.SetArtProvider(art)
            
            self.oApi.Name("nbCenter")
            self.oApi.CenterPane()
            self.oApi.PaneBorder(False)
            self.mgrMain.AddPane(wNb,self.oApi)
            
            oWid=ldmWidSizeRspWid(wNb, -1,mgr=self.mgrMain)
            wNb.AddPage(oWid,'first',True)
            
            oWid=ldmWidSizeRspWid(wNb, -1,mgr=self.mgrMain)
            wNb.AddPage(oWid,'second')
            
            #wNb.EnableTab(1, False)
            return wNb
        except:
            self.logTB()

class tstFrmAuiCenterNoteBookList(tstFrmAuiCenterNoteBook):
    def __initPanCt__(self,**kwargs):
        try:
            wNb=tstFrmAuiCenterNoteBook.__initPanCt__(self,**kwargs)
            oWid=ldmWidList(parent=wNb,iLv=0,
                        size=(200,120),sLogger='lstLog',
                        lCol=[
                            ['No',      'lf',80],
                            ['Info',    'lf',250],
                            ['Status',  'rg',60],
                        ])
            wNb.AddPage(oWid.GetWid(),'log')
            return wNb
            oApi=wx.aui.AuiPaneInfo()
            oApi.Name('lstLog Center')
            oApi.Caption("logging1")
            self.mgrMain.AddPane(oWid.GetWid(),self.oApi)
            return wNb
        except:
            self.logTB()

class tstFrmAuiCenterNoteBookThd(tstFrmAuiCenterNoteBook):
    def __initPanCt__(self,**kwargs):
        try:
            wNb=tstFrmAuiCenterNoteBook.__initPanCt__(self,**kwargs)
            oWid=ldmWidThd(parent=wNb,iLv=0,
                        size=(200,120),sLogger='thdFdr',
                        )
            wNb.AddPage(oWid.GetWid(),'thd')
            return wNb
        except:
            self.logTB()


class tstFrmAuiBottomTree(ldmWidFrmAui):
    def __initPanBt__(self,**kwargs):
        try:
            iStyle =wx.aui.AUI_NB_TOP | wx.aui.AUI_NB_TAB_SPLIT 
            iStyle|=wx.aui.AUI_NB_SCROLL_BUTTONS
            wNb=wx.aui.AuiNotebook(self.GetWid(), -1, 
                        (0,0),
                        wx.Size(430, 200),
                        style=iStyle)
            self.oApi.Name("nbBottom")
            self.oApi.Bottom()
            self.oApi.PaneBorder(False)
            self.mgrMain.AddPane(wNb,self.oApi)

            oApi=wx.aui.AuiPaneInfo()
            oApi.Name('lstLog 1')
            oApi.Caption("logging1")
            oWid=ldmWidTree(parent=self.GetWid(),iLv=0,size=(200,120),
                            sLogger='tr')
            wNb.AddPage(oWid.GetWid(),'log')
        except:
            self.logTB()

class tstFrmAuiBottomLog(ldmWidFrmAui):
    def __initPanBt__(self,**kwargs):
        try:
            self.oApi.Name('lstLog')
            self.oApi.Caption("logging")
            self.oApi.Layer(1)
            self.oApi.Position(1)
            #self.oApi.Dockable(False)
            #self.oApi.Floatable(False)
            oWid=ldmWidList(parent=self.GetWid(),iLv=0,
                        size=(200,120),sLogger='lstLog',
                        lCol=[
                            ['No',      'lf',80],
                            ['Info',    'lf',250],
                            ['Status',  'rg',60],
                        ])
            self.mgrMain.AddPane(oWid.GetWid(),self.oApi)
        except:
            self.logTB()

class tstFrmAuiLeftSizer(ldmWidFrmAui):
    def __initPanLf__(self,**kwargs):
        try:
            self.oApi.Name('szRsp2')
            self.oApi.Caption("size responder 2")
            self.oApi.Dockable(True)
            self.oApi.Row(0)
            oWid=ldmWidSizeRspWid(self.GetWid(), -1,mgr=self.mgrMain)
            self.mgrMain.AddPane(oWid,self.oApi)
        except:
            self.logTB()

class tstFrmAuiFloatingEvt(ldmWidFrmAui):
    def __initEvt__(self,**kwargs):

        #self.Bind(aui.EVT_AUITOOLBAR_TOOL_DROPDOWN, self.OnDropDownToolbarItem, id=ID_DropDownToolbarItem)
        #self.Bind(aui.EVT_AUI_PANE_CLOSE, self.OnPaneClose)
        #self.Bind(aui.EVT_AUINOTEBOOK_ALLOW_DND, self.OnAllowNotebookDnD)
        #self.Bind(aui.EVT_AUINOTEBOOK_PAGE_CLOSE, self.OnNotebookPageClose)

        #self.Bind(aui.EVT_AUI_PANE_FLOATING, self.OnFloatDock)
        #self.Bind(aui.EVT_AUI_PANE_FLOATED, self.OnFloatDock)
        #self.Bind(aui.EVT_AUI_PANE_DOCKING, self.OnFloatDock)
        #self.Bind(aui.EVT_AUI_PANE_DOCKED, self.OnFloatDock)
        
        self.wid.Bind(wx.EVT_ERASE_BACKGROUND, self.OnFrmEraseBackground)
        self.wid.Bind(wx.EVT_SIZE, self.OnFrmSize)
        self.wid.Bind(wx.EVT_CLOSE, self.OnFrmClose)
    def CreateTreeCtrl(self):
        tree = wx.TreeCtrl(self,
                    -1,
                    wx.Point(0, 0),
                    wx.Size(160, 250),
                    wx.TR_DEFAULT_STYLE | wx.NO_BORDER)

        imglist = wx.ImageList(16, 16, True, 2)
        imglist.Add(wx.ArtProvider.GetBitmap(wx.ART_FOLDER, wx.ART_OTHER, wx.Size(16, 16)))
        imglist.Add(wx.ArtProvider.GetBitmap(wx.ART_NORMAL_FILE, wx.ART_OTHER, wx.Size(16, 16)))
        tree.AssignImageList(imglist)
        root = tree.AddRoot("AUI Project", 0)
        items = []
        items.append(tree.AppendItem(root, "Item 1", 0))
        items.append(tree.AppendItem(root, "Item 2", 0))
        items.append(tree.AppendItem(root, "Item 3", 0))
        items.append(tree.AppendItem(root, "Item 4", 0))
        items.append(tree.AppendItem(root, "Item 5", 0))
        for item in items:
            tree.AppendItem(item, "Subitem 1", 1)
            tree.AppendItem(item, "Subitem 2", 1)
            tree.AppendItem(item, "Subitem 3", 1)
            tree.AppendItem(item, "Subitem 4", 1)
            tree.AppendItem(item, "Subitem 5", 1)
        tree.Expand(root)
        return tree
    def OnFloatDock(self, event):
        paneLabel = event.pane.caption
        iEvtType = event.GetEventType()
        if iEvtType == wx.aui.wxEVT_AUI_PANE_FLOATING:
            if event.pane.name == "test8" and self._veto_tree:
                event.Veto()
                return
        elif iEvtType == wx.aui.wxEVT_AUI_PANE_FLOATED:
            pass
        elif iEvtType == wx.aui.wxEVT_AUI_PANE_DOCKING:
            if event.pane.name == "test11" and self._veto_text:
                event.Veto()
                return
        elif iEvtType == wx.aui.wxEVT_AUI_PANE_DOCKED:
            pass

class tstFrmAuiList(ldmWidFrmAui):
    def __initPanCt__(self,**kwargs):
        try:
            wNb=ldmWidFrmAui.__initPanCt__(self,**kwargs)
            oWid=ldmWidList(parent=wNb,iLv=0,
                        size=(200,120),sLogger='lstRes',
                        multiple_sel=True,
                        map=[
                            ('iAct',0),
                            ['sVal',1],
                            ],
                        lCol=[
                            ['action',  'lf',80],
                            ['Value',   'lf',250],
                            ['Count',   'rg',60],
                        ])
            wNb.AddPage(oWid.GetWid(),'res')
            self.lstRes=oWid
            return wNb
        except:
            self.logTB()
    def __initPanBt__(self,**kwargs):
        try:
            self.oApi.Name('lstLog')
            self.oApi.Caption("logging")
            self.oApi.Layer(1)
            self.oApi.Position(1)
            #self.oApi.Dockable(False)
            #self.oApi.Floatable(False)
            oWid=ldmWidList(parent=self.GetWid(),iLv=0,
                        size=(200,120),sLogger='lstLog',
                        multiple_sel=True,
                        lCol=[
                            ['No',      'lf',80],
                            ['Info',    'lf',250],
                            ['Status',  'rg',60],
                        ])
            self.mgrMain.AddPane(oWid.GetWid(),self.oApi)
            
            lVal=[
                [None,['10','info 10','0x10']],
                [None,['11','info 11','0x11']],
                [None,['15','info 15','0x15']],
                ]
            oWid.SetValue(lVal)
            lVal=[
                [None,['12','info 10','0x10']],
                [None,['13','info 11','0x11']],
                ]
            #oWid.SetValue(lVal,2)
            oWid.Insert(lVal,2)
            lVal=[
                [None,['52','info 10','0x10']],
                [None,['53','info 11','0x11']],
                ]
            oWid.Insert(lVal)
            oWid.SetSelected([1,3])
            self.lstLog=oWid
        except:
            self.logTB()
    def __initCfg__(self,**kwargs):
        try:
            # +++++ beg:initialize
            sOrg='tstFrmAuiList::__initCfg__'
            self.logDbg('beg:%s'%(sOrg))
            ldmWidFrmAui.__initCfg__(self,**kwargs)
            # ----- end:initialize
            # +++++ beg:
            self.tTbrBmpSz=wx.Size(32, 32)
            # ----- end:
            # +++++ beg:
            self.setCfgWid('toolbar','bmp.iSz','32')
            # ----- end:
            # +++++ beg:toolbar file
            self.setCfgWid('tbrFile','open.tip','Open Json File')
            self.setCfgWid('tbrFile','save.tip','Save Json File')
            # ----- end:toolbar file
            # +++++ beg:toolbar edit
            self.setCfgWid('tbrEdit','cut.tip','cut')
            self.setCfgWid('tbrEdit','copy.tip','copy')
            self.setCfgWid('tbrEdit','paste.tip','paste')
            # ----- end:toolbar edit
            # +++++ beg:toolbar command
            self.setCfgWid('tbrCmd','00.enable','1')
            self.setCfgWid('tbrCmd','01.enable','1')
            self.setCfgWid('tbrCmd','02.enable','1')
            self.setCfgWid('tbrCmd','03.enable','1')
            self.setCfgWid('tbrCmd','04.enable','1')
            self.setCfgWid('tbrCmd','05.enable','1')
            self.setCfgWid('tbrCmd','06.enable','1')
            self.setCfgWid('tbrCmd','07.enable','1')
            self.setCfgWid('tbrCmd','08.enable','1')
            self.setCfgWid('tbrCmd','09.enable','1')
            # ----- end:toolbar command
            # +++++ beg:toolbar destination
            self.setCfgWid('tbrDst','checked','1')
            self.setCfgWid('tbrDst','00.enable','1')
            self.setCfgWid('tbrDst','01.enable','1')
            self.setCfgWid('tbrDst','02.enable','1')
            self.setCfgWid('tbrDst','03.enable','1')
            self.setCfgWid('tbrDst','04.enable','1')
            self.setCfgWid('tbrDst','05.enable','1')
            self.setCfgWid('tbrDst','06.enable','1')
            self.setCfgWid('tbrDst','07.enable','1')
            self.setCfgWid('tbrDst','08.enable','1')
            self.setCfgWid('tbrDst','09.enable','1')
            # ----- end:toolbar destination
            # +++++ beg:
            self.setCfgWid('toolbar','command','1')
            self.setCfgWid('menubar','command','1')
            self.setCfgWid('toolbar','destination','1')
            self.setCfgWid('menubar','destination','1')
            self.setCfgWid('frmMain','width','900')
            self.setCfgWid('frmMain','height','600')
            # ----- end:
            # +++++ beg:finalize
            self.logDbg('end:%s'%(sOrg))
            # ----- end:finalize
        except:
            self.logTB()
    def prcLstItm(self,iOfs,iCnt,iIdx,sCmd,widLst,lLog=None,lSel=None):
        try:
            self.logDbg('iIdx:%4d (%4d-%4d) sCmd:%s',
                        iIdx,iOfs,iCnt,sCmd)
            lVal=widLst.GetValue(idx=iIdx)
            self.logDbg('lVal:%r',lVal)
            sVal=widLst.GetItemStr(iIdx,1)
            self.logDbg('sVal:%r',sVal)
            tVal=widLst.GetItemTup(iIdx,1)
            self.logDbg('tVal:%r',tVal)
            return 1
        except:
            self.logTB()
            return -1
    def prcCmd(self,sCmd):
        """process command, called by event default handler
        ### parameter
            sCmd    ... command name
                cmd00   ... command button 00, tbrCmd.00
                cmd01   ... command button 00, tbrCmd.01
                ..
                cmd09   ... command button 00, tbrCmd.09
        ### return
            >0  ... okay processing done
            =0  ... okay nop
            <0  ... error
        """
        try:
            # +++++ beg:initialize
            sOrg='tstFrmAuiList::prcCmd'
            self.logDbg('beg:%s sCmd:%s',sOrg,sCmd)
            iRet=0
            # ----- end:initialize
            # +++++ beg:
            if sCmd=='cmd00':
                self.lstRes.SetValue(None,idx=-1)
            elif sCmd=='cmd01':
                lVal=[
                    [None,['cmd01','val 0']],
                    [None,['cmd01','val 1']],
                    [None,['cmd01','val 2']],
                    [None,['cmd01','val 3','2']],
                    [None,['cmd01','val 7','8']],
                ]
                self.lstRes.SetValue(lVal,idx=-1)
            elif sCmd=='cmd02':
                lVal=[
                    [None,['cmd02','val 0']],
                    [None,['cmd02','val 1']],
                    [None,['cmd02','val 2']],
                    [None,['cmd02','val 3','2']],
                    [None,['cmd02','val 7','8']],
                ]
                self.lstRes.SetValue(lVal,idx=2)
            elif sCmd=='cmd03':
                lLog=self.lstLog.GetSelected(lMap=[-1,0,2,-2])
                lVal=[]
                for lIt in lLog:
                    sInf='%03d:%s'%(lIt[3],lIt[1])
                    lVal.append([lIt[0],['cmd03',sInf,lIt[2]]])
                self.lstRes.SetValue(lVal,idx=3)
            elif sCmd=='cmd04':
                self.lstRes.DelIdx(4)
            elif sCmd=='cmd05':
                self.lstRes.DelSelected()
            elif sCmd=='cmd06':
                lLog=self.lstLog.GetSelected(lMap=[-1,0,2,-2,-3])
                lVal=[]
                for lIt in lLog:
                    sInf='%03d:%s'%(lIt[3],lIt[1])
                    dVal={
                        -1:lIt[0],
                        'iAct':'cmd06',
                        'sVal':sInf,
                        'sStat':lIt[2],
                    }
                    lVal.append(dVal)
                self.lstRes.SetValue(lVal,idx=3)
            elif sCmd=='cmd07':
                lVal=[
                    [None,['cmd07','val 0']],
                    [None,['cmd07','val 1']],
                    [None,['cmd07','val 2']],
                    [None,['cmd07','val 3','2']],
                    [None,['cmd07','val 7','8']],
                ]
                self.lstRes.Insert(lVal,idx=2)
            elif sCmd=='cmd08':
                lLog=self.lstLog.GetSelected()
                lSel=self.lstRes.GetSelected()
                self.lstRes.PrcFct(None,0,
                                   self.prcLstItm,
                                   *(sCmd,self.lstRes),
                                   **{'lLog':lLog,'lSel':lSel})
            elif sCmd=='cmd09':
                l=[2,3,4,7,8]
                self.lstRes.SetSelected(l)
            
            # ----- end:
            # +++++ beg:finalize
            self.logDbg('end:%s',sOrg)
            # ----- end:finalize
            return iRet
        except:
            self.logTB()
            return -1

class tstFrmAuiTree(tstFrmAuiList):
    SEL=[120,124]
    VAL=[
        'tag',
        {-1:0,'iNr':'0','sT':'root','sN':'name 0',
            -2:[
                {-1:100,'iNr':'1100','sT':'ref 100','sN':'name0',},
                {-1:150,'iNr':'1500','sT':'ref 150','sN':'name 50',},
                {-1:120,'iNr':'1200','sT':'ref 120','sN':'name 50',-2:[
                    {-1:201,'iNr':'2010','sT':'ref 201','sN':'name 201',},
                    {-1:202,'iNr':'2020','sT':'ref 202','sN':'name 202',},
                    {-1:203,'iNr':'2030','sT':'ref 203','sN':'name 203',},
                    {-1:204,'iNr':'2040','sT':'ref 204','sN':'name 204',},
                    ]},
                {-1:130,'iNr':'1300','sT':'ref 130','sN':'name 50',},
                {-1:121,'iNr':'1210','sT':'ref 121','sN':'name 50',},
                {-1:122,'iNr':'1220','sT':'ref 122','sN':'name 50',},
                {-1:123,'iNr':'1230','sT':'ref 123','sN':'name 50',},
                {-1:124,'iNr':'1240','sT':'ref 124','sN':'name 50',},
                {-1:125,'iNr':'1250','sT':'ref 125','sN':'name 50',},
                {-1:126,'iNr':'1260','sT':'ref 126','sN':'name 50',},
                {-1:127,'iNr':'1270','sT':'ref 127','sN':'name 50',},
                {-1:128,'iNr':'1280','sT':'ref 128','sN':'name 50',},
                {-1:131,'iNr':'1310','sT':'ref 131','sN':'name 50',},
                {-1:132,'iNr':'1320','sT':'ref 132','sN':'name 50',},
                ],
            },
        'desc',
        ]
    def __initPanCt__(self,**kwargs):
        try:
            wNb=ldmWidFrmAui.__initPanCt__(self,**kwargs)
            oWid=ldmWidTree(parent=wNb,iLv=0,iVerbose=10,
                        size=(200,120),sLogger='trRes',
                        multiple_sel=True,
                        map=[
                            ('sT',0),
                            ],
                        lCol=[
                            ['action',  'lf',80],
                            ['Value',   'lf',250],
                            ['Count',   'rg',60],
                            ],
                        value=self.VAL[1]
                        )
            wNb.AddPage(oWid.GetWid(),'res')
            #wNb.SetSelection(1)
            #wPn=wNb.GetPane('res')
            #self.CB(wPn.Show,True)#.Top().Layout(0).Row(0).Position(0)
            self.CB(wNb.SetSelection,0)
            self.trRes=oWid
            return wNb
        except:
            self.logTB()
    def __initCfg__(self,**kwargs):
        try:
            # +++++ beg:initialize
            sOrg='tstFrmAuiTree::__initCfg__'
            self.logDbg('beg:%s'%(sOrg))
            tstFrmAuiList.__initCfg__(self,**kwargs)
            # ----- end:initialize
            # +++++ beg:
            self.tTbrBmpSz=wx.Size(32, 32)
            # ----- end:
            # +++++ beg:
            # ----- end:
            # +++++ beg:finalize
            self.logDbg('end:%s'%(sOrg))
            # ----- end:finalize
        except:
            self.logTB()
    def prcCmd(self,sCmd):
        """process command, called by event default handler
        ### parameter
            sCmd    ... command name
                cmd00   ... command button 00, tbrCmd.00
                cmd01   ... command button 00, tbrCmd.01
                ..
                cmd09   ... command button 00, tbrCmd.09
        ### return
            >0  ... okay processing done
            =0  ... okay nop
            <0  ... error
        """
        try:
            # +++++ beg:initialize
            sOrg='tstFrmAuiTree::prcCmd'
            self.logDbg('beg:%s sCmd:%s',sOrg,sCmd)
            iRet=0
            # ----- end:initialize
            # +++++ beg:
            if sCmd=='cmd00':
                self.trRes.SetValue(self.VAL[1],idx=-1)
            elif sCmd=='cmd01':
                lRet=self.trRes.GetValue(ti=-1)
                self.logDbg('lRet:%r',lRet)
                lVal=[]
                for it in lRet:
                    lVal.append([None,['%r'%it,'ti %d'%it,'cmd01']])
                #for it in lVal:
                #    self.lstLog.Insert(it,idx=0)
                self.logDbg('lVal:%r',lVal)
                self.lstLog.Insert(lVal,idx=0)
            elif sCmd=='cmd02':
                l=self.trRes.GetSelections()
                self.logDbg('GetSelections l %r',l)
                lVal=[]
                iOfs=0
                for ti in l:
                    sHier=self.trRes.GetHier(ti)
                    lVal.append([None,['%d'%iOfs,'.'.join(sHier),'cmd02']])
                    iOfs+=1
                self.lstLog.Insert(lVal,idx=0)
            elif sCmd=='cmd03':
                l=self.trRes.GetSelected()
                self.logDbg('GetSelected l %r',l)
                lVal=[]
                for it in l:
                    lVal.append([None,['%r'%it[0],'.'.join(it[1]),'cmd03']])
                self.logDbg('lVal:%r',lVal)
                self.lstLog.Insert(lVal,idx=0)
            elif sCmd=='cmd04':
                lSel=[150,123,202]
                self.trRes.SetSelected(lSel)
            elif sCmd=='cmd05':
                l=self.trRes.GetValue(lMap=[-1,0,1,-2])
                self.logDbg('GetValue l %r',l)
            elif sCmd=='cmd06':
                l=self.trRes.GetValue(lMap=[-1,0,'iNr','sN'])
                self.logDbg('GetValue l %r',l)
            elif sCmd=='cmd07':
                dVal={-1:400,'iNr':'400','sT':'cmd 400','sN':'command07 400'}
                lH=['root','ref 100']
                l=self.trRes.SetValue(dVal,lHier=lH)
                dVal={-1:300,'iNr':'300','sT':'cmd 300','sN':'command07 300',
                    -2:[
                        {-1:301,'iNr':'301','sT':'cmd 301','sN':'command 301'},
                        {-1:302,'iNr':'302','sT':'cmd 302','sN':'command 302'},
                    ]
                    }
                lH=['root','ref 120','ref 201']
                l=self.trRes.SetValue(dVal,lHier=lH)
                self.logDbg('SetValue l %r',l)
            elif sCmd=='cmd08':
                dVal={-1:211,'iNr':'211','sT':'cmd 211','sN':'command07 211',
                    -2:[
                        {-1:301,'iNr':'301','sT':'cmd 301','sN':'command 301'},
                        {-1:302,'iNr':'302','sT':'cmd 302','sN':'command 302'},
                    ]
                    }
                lH=['root','ref 120','ref 202']
                l=self.trRes.SetValue([dVal],lHier=lH,bChildren=True)
                self.logDbg('SetValue l %r',l)
            elif sCmd=='cmd09':
                dVal={-1:231,'iNr':'231','sT':'cmd 231','sN':'command07 231',
                    -2:[
                        {-1:501,'iNr':'501','sT':'cmd 501','sN':'command 501'},
                        {-1:502,'iNr':'502','sT':'cmd 502','sN':'command 502'},
                    ]
                    }
                lH=['root','ref 120','ref 203']
                l=self.trRes.SetValue(dVal,lHier=lH,bChildren=True)
                self.logDbg('SetValue l %r',l)
            
            # ----- end:
            # +++++ beg:finalize
            self.logDbg('end:%s',sOrg)
            # ----- end:finalize
            return iRet
        except:
            self.logTB()
            return -1
    def doInpCmd(self,evt):
        try:
            v={ 'data':evt.GetData(),
                'cmd':evt.GetCmd(),
                'mth':'doInpCmd',
                }
            self.logDbg(v)
            self.addRes(v)
        except:
            self.logTB()
    def _getTreeEvt(self,evt):
            evt.Skip()
            ti=evt.GetItem()
            tr=evt.GetEventObject()
            if ti.IsOk():
                dat=tr.GetPyData(ti)
                txt=tr.GetItemText(ti)
            else:
                dat='--'
                txt='--'
            v={ 'dat':dat,
                'txt':txt,
                'evt':evt.__class__.__name__,
                'evtId':evt.GetEventType(),
                'evtName':self.wid0.GetSysEvtNameByEvt(evt),
                #'cltData':evt.GetClientData(),
                }
            return v
    def doTreeEvt(self,evt):
        try:
            v=self._getTreeEvt(evt)
            self.logDbg(v)
            self.addRes(v)
        except:
            self.logTB()
    def doTreeSel(self,evt):
        try:
            v=self._getTreeEvt(evt)
            v['mth']='doTreeSel'
            self.logDbg(v)
            self.addRes(v)
        except:
            self.logTB()
    def doTreeCol(self,evt):
        try:
            v=self._getTreeEvt(evt)
            v['mth']='doTreeCol'
            self.logDbg(v)
            self.addRes(v)
        except:
            self.logTB()
    def doTreeExp(self,evt):
        try:
            v=self._getTreeEvt(evt)
            v['mth']='doTreeExp'
            self.logDbg(v)
            self.addRes(v)
        except:
            self.logTB()
    def doTreeMenu(self,evt):
        try:
            v=self._getTreeEvt(evt)
            v['mth']='doTreeMenu'
            self.logDbg(v)
            self.addRes(v)
        except:
            self.logTB()
    def doTreeMClk(self,evt):
        try:
            v=self._getTreeEvt(evt)
            v['mth']='doTreeMClk'
            self.logDbg(v)
            self.addRes(v)
        except:
            self.logTB()
    def doTreeRClk(self,evt):
        try:
            v=self._getTreeEvt(evt)
            v['mth']='doTreeRClk'
            self.logDbg(v)
            self.addRes(v)
        except:
            self.logTB()
