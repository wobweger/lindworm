#----------------------------------------------------------------------------
# Name:         test_ldmStorageFolderFrm.py
# Purpose:      unit test of ldmStorageFolderFrm.py
#
# Author:       Walter Obweger
#
# Created:      20200403
# CVS-ID:       $Id$
# Copyright:    (c) 2020 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

import sys
sys.path.insert(1,'.')

import unittest   # The test framework

import lindworm.logUtil as logUtil
import lindworm.ldmStorageFolderFrm as UuT

try:
    import UuT_cfg
    isSkipLarge=UuT_cfg.isSkipLarge()
    isSkipGUI=UuT_cfg.isSkipGUI()
    iLgLv=UuT_cfg.getLogLevel()
except:
    import logging
    isSkipLarge=1
    isSkipGUI=1
    iLgLv=logging.ERROR


iLogInit=0

def initLog(sLogFN,sLogger=''):
    global iLogInit
    #print('sLogFN:%s'%(sLogFN))
    if iLogInit==0:
        logUtil.logInit(sLogFN,sLogger=sLogger,
                    iLevel=iLgLv)
        iLogInit=1

class TestBase(unittest.TestCase):
    def setUp(self):
        # +++++ beg:init logging
        initLog("./x_log/test_ldmStorageFolderFrm.log",sLogger=None)
        # ----- end:init logging
        # +++++ beg:initialize
        self.sSrcDN             ='./x_test/40_dat'
        self.sBldDN             ='./x_test/build'
        self.iShaMB             =1
        self.sBldFN             ='fdrBld.json'
        self.sCfgFN             ='./x_test/ldmStorageFolderCfg.json'
        self.iVerbose           =20
        # ----- end:initialize
        # +++++ beg:construct object
        # ----- end:construct object
    def tearDown(self):
        pass
    def testMain(self):
        if isSkipGUI==False:
            # +++++ beg:exec gather 
            iRet=UuT.main([
                '--log',    None,
                '--srcDN',  self.sSrcDN,
                '--bldDN',  self.sBldDN,
                '--bldFN',  self.sBldFN,
                '--cfgFN',  self.sCfgFN,
                '-v',str(self.iVerbose),
                ])
            # ----- end:exec gather
            self.assertEqual(iRet,7)
        else:
            logUtil.logDbg('testMain skip GUI test')

class TestDft(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        # ----- end:initialize
    def testMain(self):
        if isSkipGUI==False:
            # +++++ beg:exec gather 
            iRet=UuT.main(['--log',None])
            # ----- end:exec gather
            self.assertEqual(iRet,7)
        else:
            logUtil.logDbg('testMain skip GUI test')


class TestBaseLoc(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sBldFN             ='0'
        #self.sCfgFN             ='./x_test/ldmStorageFolderCfg_0.json'
        # ----- end:initialize

class TestBaseUTC(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sBldFN             ='1'
        # ----- end:initialize

class TestBaseOut(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sBldFN             ='TestBaseOut'
        # ----- end:initialize

class TestBaseOutJson(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sBldFN             ='TestBaseOut.json'
        # ----- end:initialize

class TestBase40(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sSrcDN             ='./x_test/40_dat'
        self.sBldFN             ='TestBase.40_dat'
        # ----- end:initialize

class TestBase41(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sSrcDN             ='./x_test/41_dat'
        self.sBldFN             ='TestBase.41_dat'
        # ----- end:initialize

class TestDat07Eng(TestBase):
    def setUp(self):
        TestBase.setUp(self)
        # +++++ beg:initialize
        self.sCfgFN             ='./x_test/ldmStorageFolderCfgTimes.json'
        self.sSrcDN             ='F:\dat05'
        self.sBldFN             ='dat07.eng_dat05'
        # ----- end:initialize

if __name__ == '__main__':
    unittest.main()
