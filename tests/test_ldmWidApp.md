## TestBase

## TestDft

## TestCenterRow

center pane with dockable size responder and tree

+ `tstFrmAuiCenter.tstFrmAuiCenterRows`
  + `ldmWidSizeRspWid`
  + `ldmWidSizeRspWid` dockable
  + `ldmWidTree` dockable

## TestCenterNoteBook

+ `tstFrmAuiCenter.tstFrmAuiCenterNoteBook`

## TestCenterNoteBookList

+ `tstFrmAuiCenter.tstFrmAuiCenterNoteBookList`

## TestCenterNoteBookThd

+ `tstFrmAuiCenter.tstFrmAuiCenterNoteBookThd`

## TestCenterBottomTree

+ `tstFrmAuiCenter.tstFrmAuiBottomTree`

## TestCenterBottomLog

+ `tstFrmAuiCenter.tstFrmAuiBottomLog`

## TestTbrFile

+ `tstFrmAuiCenter.tstFrmAuiTbrFile`

## TestCenterLeftSizer

+ `tstFrmAuiCenter.tstFrmAuiLeftSizer`

## TestTbrDstRadio

toolbar destination with radio buttons

+ `tstFrmAuiCenter.tstFrmAuiTbrDstRadio`

destination

0. FindTool, log GetState
1. FindTool, log GetState

## TestTbrDstCheck

toolbar destination with checkbox

+ `tstFrmAuiCenter.tstFrmAuiTbrDstCheck`

destination

0. FindTool, log GetState
1. FindTool, log GetState

## TestList

simple GUI test of `ldmWidList`

+ `tstFrmAuiCenter.tstFrmAuiList`
  + center pane
    + lstRes `ldmWidList`
  + bottom pane
    + lstLog `ldmWidList`

commands

0. empty widget using SetValue
1. SetValue append 5 values
2. SetValue add 5 values at index 2
3. get selected values from lstLog and SetValue at index 3 using lists
4. DelIdx delete index 4
5. DelSelected, delete selected
6. get selected values from lstLog and SetValue at index 3 using list of dictionaries
7. Insert 5 values
8. GetSelected of lstLog and lstRes and apply prcLstItm using PrcFct
9. SetSelected by [2,3,4,7,8]

## TestListCfgDst

inherited by TestList and use json configuration file `./x_test/ldmWidAppCfgDst.json`


## TestListCfgTbrHid

inherited by TestList and use json configuration file `./x_test/ldmWidAppCfgTbrHid.json`

## TestTree

simple GUI test of `ldmWidTree`

+ `tstFrmAuiCenter.tstFrmAuiTree`

commands

0. SetValue to self.VAL[1]
1. GetValue and add to lstRes by Insert
2. GetSelections and add to lstRes
3. GetSelected and add to lstRes
4. SetSelected to [150,123,202]
5. GetValue mapping lMap=[-1,0,1,-2]
6. GetValue mapping lMap=[-1,0,'iNr','sN']
7. SetValue at ['root','ref 100'], SetValue at ['root','ref 120','ref 201']
8. SetValue at ['root','ref 120','ref 202'] children
9. SetValue at ['root','ref 120','ref 203']
