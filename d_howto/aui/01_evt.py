

class frm:
    def OnSrcDnEnter(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        event.Skip()
        try:
            # +++++ beg:get DN
            sSrcDN=self.txtSrcDN.GetValue()
            if os.path.exists(sSrcDN):
                self.sSrcDN=sSrcDN
                self.oFld.log('OnSrcOnEnter sSrcDN:%s ok',sSrcDN)
            else:
                self.oFld.logErr('OnSrcOnEnter sSrcDN:%s does not exist',sSrcDN)
            # ----- end:get DN
        except:
            self.oFld.logTB()
    def OnSrcDN(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        event.Skip()
        try:
            # +++++ beg:get DN
            sSrcDN=self.txtSrcDN.GetValue()
            iRet,sSrcDN=ldmGui.getDN(sSrcDN,self,
                         'choose source directory')
            if iRet>0:
                self.sSrcDN=sSrcDN
                self.txtSrcDN.SetValue(self.sSrcDN)
                self.oFld.log('fin:OnSrcDN iRet:%d sSrcDN:%s',
                            iRet,sSrcDN)
            # ----- end:get DN
        except:
            self.oFld.logTB()

    def OnBldDnEnter(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        event.Skip()
        try:
            # +++++ beg:get DN
            sBldDN=self.txtBldDN.GetValue()
            if os.path.exists(sBldDN):
                self.sBldDN=sBldDN
                self.oFld.log('OnBldOnEnter sBldDN:%s ok',sBldDN)
            else:
                self.oFld.logErr('OnBldOnEnter sBldDN:%s does not exist',sBldDN)
            # ----- end:get DN
        except:
            self.oFld.logTB()
    def OnBldDN(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        event.Skip()
        try:
            # +++++ beg:get DN
            sBldDN=self.txtBldDN.GetValue()
            iRet,sBldDN=ldmGui.getDN(sBldDN,self,
                         'choose build directory')
            if iRet>0:
                self.sBldDN=sBldDN
                self.txtBldDN.SetValue(self.sBldDN)
                self.oFld.log('fin:OnBldDN iRet:%d sBldDN:%s',
                            iRet,sBldDN)
            # ----- end:get DN
        except:
            self.oFld.logTB()

    def OnBldFnEnter(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        event.Skip()
        try:
            # +++++ beg:get DN
            sBldFN=self.txtBldFN.GetValue()
            self.sBldFN=sBldFN
            self.oFld.log('OnBldFnEnter sBldFN:%s ok',sBldFN)
        except:
            self.oFld.logTB()

    def OnBldFN(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        event.Skip()
        try:
            # +++++ beg:get DN
            sBldFN=self.txtBldFN.GetValue()
            # ----- end:get DN
            iRet,sBldFN=ldmGui.getFN(sBldFN,self,
                         'choose build file name',
                         lWildCard=self.WILDCARD_JSON,
                         oLog=self.oFld)
            if iRet>0:
                sBldDN,sBldFN=ldmGui.getSplitFN(sBldFN)
                if (sBldDN is not None) and (sBldFN is not None):
                    self.sBldDN,self.sBldFN=sBldDN,sBldFN
                    self.txtBldDN.SetValue(self.sBldDN)
                    self.txtBldFN.SetValue(self.sBldFN)
                    self.oFld.log('fin:OnBldFN iRet:%d sBldFN:%s',
                                iRet,sBldFN)
        except:
            self.oFld.logTB()

    def OnLogFnEnter(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        print("Event handler 'OnLogFnEnter' not implemented!")
        event.Skip()

    def OnLogFN(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        print("Event handler 'OnLogFN' not implemented!")
        event.Skip()

    def OnCfgFnEnter(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        print("Event handler 'OnCfgFnEnter' not implemented!")
        event.Skip()
    def OnCfgFN(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        event.Skip()
        try:
            sCfgFN=self.txtCfgFN.GetValue()
            iRet,sCfgFN=ldmGui.getFN(sCfgFN,self,
                         'choose configuration file name',
                         lWildCard=self.WILDCARD_JSON,
                         oLog=self.oFld)
            if iRet>0:
                self.sCfgFN=sCfgFN
                self.txtCfgFN.SetValue(self.sCfgFN)
                self.oFld.log('fin:OnCfgFN iRet:%d sCfgFN:%s',
                            iRet,sCfgFN)
        except:
            self.oFld.logTB()

