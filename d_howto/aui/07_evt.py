class dmy:
    def OnStop(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        event.Skip()
        try:
            # +++++ beg:
            iRet=0
            sOrg='ldmStorageFolderViewFrm::OnStop'
            self.oFdr.logDbg('beg:%s',sOrg)
            # ----- end:
            self.oThd.Stop()
            # +++++ beg:
            self.oFdr.logDbg('end:%s iRet:%d',sOrg,iRet)
            # ----- end:
        except:
            self.oFdr.logTB()

    def OnDel(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        print("Event handler 'OnDel' not implemented!")
        event.Skip()

    def OnAdd(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        print("Event handler 'OnAdd' not implemented!")
        event.Skip()

    def OnOpn(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        print("Event handler 'OnOpn' not implemented!")
        event.Skip()

    def OnCpy00(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        print("Event handler 'OnCpy00' not implemented!")
        event.Skip()

    def OnCpy01(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        print("Event handler 'OnCpy01' not implemented!")
        event.Skip()

    def OnCpy02(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        print("Event handler 'OnCpy02' not implemented!")
        event.Skip()

    def OnCpy03(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        print("Event handler 'OnCpy03' not implemented!")
        event.Skip()
    def OnCfgFnEnter(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        print("Event handler 'OnCfgFnEnter' not implemented!")
        event.Skip()


    def OnShaMbChoice(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        event.Skip()
        try:
            sVal=self.chcShaMB.GetString(self.chcShaMB.GetSelection())
            iIdx=self.chcShaMB.GetSelection()
            self.oFdr.logDbg('OnShaMbChoice sVal:%s iIdx:%d',sVal,iIdx)
            if iIdx==0:
                self.spnShaMB.SetValue(0)
                self.iShaMB=0
            elif iIdx==8:
                self.spnShaMB.SetValue(-1)
                self.iShaMB=-1
            elif iIdx==1:
                self.spnShaMB.SetValue(1)
                self.iShaMB=1
            elif iIdx==2:
                self.spnShaMB.SetValue(2)
                self.iShaMB=2
            elif iIdx==3:
                self.spnShaMB.SetValue(4)
                self.iShaMB=4
            elif iIdx==4:
                self.spnShaMB.SetValue(8)
                self.iShaMB=8
            elif iIdx==5:
                self.spnShaMB.SetValue(16)
                self.iShaMB=16
            elif iIdx==6:
                self.spnShaMB.SetValue(32)
                self.iShaMB=32
            elif iIdx==7:
                self.spnShaMB.SetValue(64)
                self.iShaMB=64
        except:
            self.oFdr.logTB()

    def OnShaMbSpin(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        event.Skip()
        try:
            iVal=self.spnShaMB.GetValue()
            self.oFdr.logDbg('OnShaMbSpin iVal:%d',iVal)
            self.iShaMB=iVal
            #["disabled", "01 MB", "02 MB", "04 MB", "08 MB", "16 MB", "32 MB", "64 MB", "all data"]
            if iVal==0:
                self.chcShaMB.SetSelection(0)
            elif iVal<0:
                self.chcShaMB.SetSelection(8)
            elif iVal==1:
                self.chcShaMB.SetSelection(1)
            elif iVal==2:
                self.chcShaMB.SetSelection(2)
            elif iVal==4:
                self.chcShaMB.SetSelection(3)
            elif iVal==8:
                self.chcShaMB.SetSelection(4)
            elif iVal==16:
                self.chcShaMB.SetSelection(5)
            elif iVal==32:
                self.chcShaMB.SetSelection(6)
            elif iVal==64:
                self.chcShaMB.SetSelection(7)
            
        except:
            self.oFdr.logTB()

    def OnShaMbEnter(self, event):  # wxGlade: ldmStorageFolderViewFrm.<event_handler>
        print("Event handler 'OnShaMbEnter' not implemented!")
        event.Skip()
