
from optparse import OptionParser
from argparse import ArgumentParser

def main(args=None):
    # +++++ beg:
    # ----- end:
    
    # +++++ beg:init
    iRet=0
    iVerbose=5                                          # 20190624 wro:set default verbose level
    # ----- end:init
    # +++++ beg:define CLI arguments
    usage = "usage: %prog [options]"
    #oArg.addOpt('')
    oParser=ArgumentParser()#(usage,version="%prog "+__version__)
    oParser.add_argument('--cfgFN',
            default='ldmStorageFolderCfg.json',
            help='configuration file',metavar='pyGatherMDCfg.json',
            dest='sCfgFN'
            )
    oParser.add_argument('--srcDN',
            default='./',
            help='source folder',metavar='path/to/folder/to/read',
            dest='sSrcDN',
            )
    oParser.add_argument('--bldDN',
            default='./',
            help='build directory',metavar='path/to/output/folder',
            dest='sBldDN',
            )
    oParser.add_argument('--bldFN',
            default='',
            help='build file',metavar='sng.json',
            dest='sBldFN',
            )
    oParser.add_argument('--log',
            default='./log/ldmStorageFolder.log',
            help='log filename',metavar='./log/ldmStorageFolder.log',
            dest='sLogFN',
            )
    oParser.add_argument("-v", type=int, nargs='?' , 
            default=0,
            dest="verbose", 
            )
    #oParser.add_argument("-q", action="store_false", dest="verbose", default=True)

    opt=oParser.parse_args(args)
    #opt.verbose=1
    print('verbose',opt.verbose)
    if opt.verbose:
        print("config FN:     %r"%(opt.sCfgFN))
        print("source DN:     %r"%(opt.sSrcDN))
        print(" build DN:     %r"%(opt.sBldDN))
        print(" build FN:     %r"%(opt.sBldFN))
        iVerbose=20
    global gOpt
    gOpt=opt
    # ----- end:parse command line
    # +++++ beg:prepare logging
    if opt.sLogFN is not None:
        import lindworm.logUtil as logUtil
        logUtil.logInit(oArg.sLogFN,iLevel=logging.DEBUG)
    # ----- end:prepare logging
    # +++++ beg:
    iRet+=1
    # ----- end:
    return iRet

if __name__ == "__main__":
    # +++++ beg:call entry point
    main(args=None)
    # ----- end:call entry point
