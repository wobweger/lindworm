class dmy:
    def OnThdNty(self,evt):
        evt.Skip()
        try:
            # +++++ beg:
            #iSched=evt.oNty.GetSched()
            sStatOfs=evt.oNty.GetStatusOfs()
            iVal=evt.oNty.GetNormalized()
            sPhase=evt.oNty.GetPhase()
            sStatus=evt.oNty.GetStatus()
            self.gagDetail.SetValue(int(iVal))
            self.txtPhase.SetValue(sPhase)
            self.txtStatus.SetValue(sStatus)
            if evt.oNty.IsActive():
                iIdx=self.lcrLog.InsertItem(0,sStatOfs)
                self.lcrLog.SetItem(iIdx,1,sStatus)
            # ----- end:
        except:
            self.oFld.logTB()
    def OnStart(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        event.Skip()
    def OnStop(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        event.Skip()
        try:
            # +++++ beg:
            iRet=0
            sOrg='ldmStorageFolderFrm::OnStop'
            self.oFld.logDbg('beg:%s',sOrg)
            # ----- end:
            self.oThd.Stop()
            # +++++ beg:
            self.oFld.logDbg('end:%s iRet:%d',sOrg,iRet)
            # ----- end:
        except:
            self.oFld.logTB()
