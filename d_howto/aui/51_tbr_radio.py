class dmy:
    def __initTbrCmd__(self,**kwargs):
        try:
            # +++++ beg:initialize
            sOrg='ldmStorageFolderFrm::__initTbrDst__'
            self.logDbg('beg:%s',sOrg)
            # ----- end:initialize
            # +++++ beg:
            oSz=self.tTbrBmpSz
            iStyle =wx.aui.AUI_TB_DEFAULT_STYLE
            tbr = wx.aui.AuiToolBar(self.GetWid(),
                        -1,
                        wx.DefaultPosition,
                        size=oSz or wx.DefaultSize,
                        style=iStyle)
            #tbr.SetToolBitmapSize(oSz)
            
            bmp=wx.ArtProvider.GetBitmap(wx.ART_GO_FORWARD,size=oSz)
            w=tbr.AddTool(self.ID_CMD+0,
                        label="00",
                        bitmap=bmp,
                        kind=wx.ITEM_RADIO,
                        short_help_string='destination 00')
            self.BindEvent('mn',self.OnCmd00,w)
            #AUI_BUTTON_STATE_NORMAL
            #AUI_BUTTON_STATE_HOVER
            #AUI_BUTTON_STATE_PRESSED
            #AUI_BUTTON_STATE_DISABLED
            #AUI_BUTTON_STATE_HIDDEN
            w.SetState(wx.aui.AUI_BUTTON_STATE_CHECKED)
            
            bmp=wx.ArtProvider.GetBitmap(wx.ART_GOTO_FIRST,size=oSz)
            w=tbr.AddTool(self.ID_CMD+1,
                        label="01",
                        bitmap=bmp,
                        kind=wx.ITEM_RADIO,
                        short_help_string='destination 01')
            self.BindEvent('mn',self.OnCmd01,w)
            
            oApi=wx.aui.AuiPaneInfo().Name('tbrCmd')
            oApi.Caption('command toolbar')
            oApi.ToolbarPane().Top()
            #oApi.GripperTop().TopDockable(False).BottomDockable(False)
            self.mgrMain.AddPane(tbr,oApi)
            # ----- end:
            # +++++ beg:finalize
            self.logDbg('end:%s'%(sOrg))
            # ----- end:finalize
        except:
            self.logTB()
