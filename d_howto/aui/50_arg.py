
from lindworm.ldmArg import ldmArg

def main(args=None):
    # +++++ beg:
    # ----- end:
    
    # +++++ beg:init
    iRet=0
    iVerbose=5                                          # 20190624 wro:set default verbose level
    # ----- end:init
    # +++++ beg:define CLI arguments
    usage = "usage: %prog [options]"
    oArg=ldmArg(sUsage=usage,sVer=__version__,iVerbose=10)
    oArg.addOpt('sCfgFN',
            sDft='ldmStorageFolderCfg.json',
            sHlp='configuration file',
            sVerbose='config FN',
            sMeta='pyGatherMDCfg.json')
    oArg.addOpt('sSrcDN',
            sDft='./',
            sHlp='source folder',
            sVerbose='source DN',
            sMeta='path/to/folder/to/read')
    oArg.addOpt('sBldDN',
            sDft='./',
            sHlp='build directory',
            sVerbose='build DN',
            sMeta='path/to/output/folder')
    oArg.addOpt('sBldFN',
            sDft='./',
            sHlp='build directory',
            sVerbose='build FN',
            sMeta='path/to/output/folder')
    oArg.addOpt('sShaMbInt',
            sDft='1',
            sHlp='size boundary;-1 <= x <= 128',
            sVerbose='limit',
            sMeta='full')
    oArg.addOpt('sShaMbChc',
            sDft='1 MB',
            sHlp='select limit;off|1 MB|2 MB|4 MB|8 MB|16 MB|32 MB|64 MB|full',
            sVerbose='limit',
            sMeta='full')
