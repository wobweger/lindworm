class dmy:
    def OnShaMbSpin(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        event.Skip()
        try:
            iVal=self.spnShaMB.GetValue()
            self.oFld.logDbg('OnShaMbSpin iVal:%d',iVal)
            self.iShaMB=iVal
            #["disabled", "01 MB", "02 MB", "04 MB", "08 MB", "16 MB", "32 MB", "64 MB", "all data"]
            if iVal==0:
                self.chcShaMB.SetSelection(0)
            elif iVal<0:
                self.chcShaMB.SetSelection(8)
            elif iVal==1:
                self.chcShaMB.SetSelection(1)
            elif iVal==2:
                self.chcShaMB.SetSelection(2)
            elif iVal==4:
                self.chcShaMB.SetSelection(3)
            elif iVal==8:
                self.chcShaMB.SetSelection(4)
            elif iVal==16:
                self.chcShaMB.SetSelection(5)
            elif iVal==32:
                self.chcShaMB.SetSelection(6)
            elif iVal==64:
                self.chcShaMB.SetSelection(7)
            
        except:
            self.oFld.logTB()

    def OnShaMbEnter(self, event):  # wxGlade: ldmStorageFolderFrm.<event_handler>
        print("Event handler 'OnShaMbEnter' not implemented!")
        event.Skip()
