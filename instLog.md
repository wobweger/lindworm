### install log

this section list installation logs, which may be of
help. Installation of wxPython is a bit more
difficult because it's not pure python, but still
worth going through all that trouble.

**20200805 CentOS 8:**

```shell
[root@sokrates lindworm]#  pip3 install -U     -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/centos-8     wxPython
Collecting wxPython
  Downloading https://extras.wxpython.org/wxPython4/extras/linux/gtk3/centos-8/wxPython-4.1.0-cp36-cp36m-linux_x86_64.whl (144.9MB)
    100% |████████████████████████████████| 144.9MB 12kB/s 
Requirement already up-to-date: numpy; python_version >= "3.0" in /usr/local/lib64/python3.6/site-packages (from wxPython)
Collecting six (from wxPython)
  Downloading https://files.pythonhosted.org/packages/ee/ff/48bde5c0f013094d729fe4b0316ba2a24774b3ff1c52d924a8a4cb04078a/six-1.15.0-py2.py3-none-any.whl
Requirement already up-to-date: pillow in /usr/local/lib64/python3.6/site-packages (from wxPython)
Installing collected packages: six, wxPython
Successfully installed six-1.15.0 wxPython-4.1.0
[root@sokrates lindworm]# 
```
