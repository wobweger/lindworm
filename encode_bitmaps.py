#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20200419
# CVS-ID:       $Id$
# Copyright:    (c) Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

import sys,os
from wx.tools import img2py

def conv2posix(s):
    return s.replace('\\','/')

sCWD=conv2posix(os.getcwd())
print('current working directory',sCWD)

sModPath=''

print('arguments',sys.argv)
iCntArg=len(sys.argv)
if iCntArg>1:
    sOrig=sys.argv[1]
else:
    sOrig='bitmaps'
print('','cnt',iCntArg)
if iCntArg>2:
    sDN=sys.argv[2]
    print('search DN',sDN)
    if len(sDN)>0:
        sTmpDN=os.path.join(sCWD,sDN)
        l=os.listdir(sTmpDN)
        iOfs=0
        for sFN in l:
            if iOfs==0:
                sFmt='    "-u -i -n %-16s  %-24s    %s.py",'
            else:
                sFmt='    "-a -u -n %-16s  %-24s    %s.py",'
            iSuf=sFN.rfind('.')
            if iSuf>0:
                sN=sFN[:iSuf]
                s=sFmt%(sN,'/'.join([sDN,sFN]),sDN)
                iOfs+=1
                print(s)
        #print l
else:
    sMod=sModPath.replace('/','.')
    modBit=__import__(sMod+"encode_%s"%sOrig,globals(),locals(),[""])


    for line in modBit.command_lines:
        args = line.split()
        print('args',args)
        img2py.main(args)


